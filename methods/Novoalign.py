import os
import subprocess
import sys
import threading
import AnnotationMethod
import Utility

novoalign_path = "/home/andruscha/bin/novocraft/novoalign"
novoindex_path = "/home/andruscha/bin/novocraft/novoindex"

class Novoalign(AnnotationMethod.AnnotationMethod):
	name = "Novoalign"

	def __init__(self, reads, reference, outputdir, tmp_dir, threads, parameters="", indexdirs=[], bg=False):
		AnnotationMethod.AnnotationMethod.__init__(self, reads, reference, outputdir, tmp_dir, threads, parameters, indexdirs, bg)

	def run(self):
		outputdir = self.outputdir+"/"+self.name
		if not os.path.isdir(outputdir):
			os.makedirs(outputdir)

		shortref = Utility.short_file_name_without_extension(self.reference)
		sys.stdout.write("Novoaligning reads against "+shortref+"\n")

		# look for an existing index
		index = check_for_index(self.reference, [os.path.dirname(self.reference)]+self.indexdirs)

		# if index = None, build an index, otherwise use the found index
		if not index:
			sys.stdout.write("Building Novoalign index\n")
			index = build_index(self.reference, outputdir)
		else:
			sys.stdout.write("Found Novoalign index\n")

		# now start aligning fo' real / chuck stuff, start single novoalign processes, merge stuff afterwards
		# threaded alignment against the reference, variables
		cores = self.threads
		ram = Utility.calculate_usable_ram()
		chunk_size = (os.path.getsize(self.reads)/cores)/2 # since chunk_size for the files is not actually the size files will have, we use a safe approach here, halfing the size again
		max_chunk_size = (ram/cores)
		if chunk_size > max_chunk_size: # we want the chunks to be pretty small so every core gets something to do but we don't them to exceed our upper limit in any case (shouldn't really happen anyway)
			chunk_size = max_chunk_size

		# chunk the input file
		chunk_list = Utility.split_fastq_file(self.reads, chunk_size)
		semaphore = threading.BoundedSemaphore(cores)
		jobs = []
		counter = 0

		#
		file_list = []
		for each in chunk_list:
			jobs.append(NovoalignJob(each, self.reference, self.tmp_dir+"/"+shortref+"_"+str(counter)+".sam", semaphore, self.parameters, self.bg))
			file_list += [self.tmp_dir+"/"+shortref+"_"+str(counter)+".sam"] # building file list for later concatenation and deletion
			counter += 1

		for job in jobs:
			job.start()

		for job in jobs:
			job.join()

		# merging the output from threaded alignments
		Utility.concatenate_files(file_list, outputdir+"/"+shortref+".sam") # should be sorted already since we start with a sorted file list

		# deleting temporary files
		for each in chunk_list:
			os.remove(each)
		for each in file_list:
			os.remove(each)

		# sam to smp splitting
		(annotated_reads, unannotated_reads) = Utility.sam_to_smp_splitter(outputdir+"/"+shortref+".sam", outputdir)

		# delete obsolete sam file
		os.remove(outputdir+"/"+shortref+".sam")

		self.output = (annotated_reads, unannotated_reads)
		return self.output

class NovoalignJob(threading.Thread):
	def __init__(self, reads, reference, outputfile, semaphore, parameters="", bg=False):
		threading.Thread.__init__(self)
		self.reads = reads
		self.reference = reference
		self.outputfile = outputfile
		self.semaphore = semaphore
		self.parameters = parameters
		self.bg = bg

	def run(self):
		self.semaphore.acquire()
		subprocess.call(novoalign_path+" -f "+self.reads+" -d "+self.reference+" -o SAM > "+self.outputfile, shell=True)
		self.semaphore.release()

def check_for_index(reference, list_of_dirs):
	# checks a given list of directories for existance of an index for the given reference, returns path to index if
	# any exists, otherwise returns None
	references = [Utility.short_file_name_without_extension(reference), os.path.basename(reference)]
	subfolders = ["novo", "."]

	for directory in list_of_dirs:
		for subfolder in subfolders:
			for ref in references:
				if os.path.isfile(directory+"/"+subfolder+"/"+ref+".ndx"):
					return directory+"/"+subfolder+"/"+ref+".ndx"

def build_index(reference, outputdir):
	shortref = Utility.short_file_name_without_extension(reference)
	call = novoindex_path+" -t "+str(self.threads)+" -n "+shortref+" "+os.path.abspath(outputdir)+"/"+shortref+".ndx "+os.path.abspath(reference)
	#sys.stdout.write("Building Novoalign index with: "+call+"\n") # novoindex gives the invoking command out by itself
	subprocess.call(call, shell=True)
	return os.path.abspath(outputdir)+"/"+shortref+".ndx"