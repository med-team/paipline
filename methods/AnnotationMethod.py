#  Parent class for all annotation methods which should be placed in the methods subfolder/package

import os

class AnnotationMethod(object):
	name = "AnnotationMethod" # a name "tag" for the annotation method class, usually the class name, is used for naming subfolders and such
	iterative = False # a variable defining if the iterative part of the main program must be run with this method, currently only necessary for bowtie2

	def __init__(self, reads, reference, outputdir, tmp_dir, threads, parameters="", indexdirs=[], bg=False):
		self.reads = os.path.abspath(reads) # all methods must take reads in fastq format
		self.reference = os.path.abspath(reference) # all methods must take a reference in fasta format
		self.outputdir = os.path.abspath(outputdir) # output directory, actually the methods should put their output in a subdirectory named like the method
		self.tmp_dir = tmp_dir # a path to a directory to be used for temporary file operations
		self.threads = threads # number of threads to be used if parts of this method are threadable
		self.parameters = parameters # an optional parameter that can define parameters with which to run the called annotation program
		self.indexdirs = [os.path.abspath(x) for x in indexdirs] # an optional parameter that can give directories with prebuilt indices that this method might use to save some time
		self.output = [] # a variable that stores the paths to the annotated reads and unannotated reads smp files that every annotation module should produce
		self.bg = bg # sets a boolean to determine whether this is a background analysis or not (some methods might need other parameters for bg searches)

	def run(self, gz=False):
		# the main method that every child class of this should have, taking all the parameters this object was created with and producing 2 smp files,
		# one with annotated reads, the other one with unannotated reads in the subfolder named after the method performed in the output directory
		print "This class does not provide any annotation methods, please use child classes instead!"

def check_for_index(reference, list_of_dirs):
	# every annotation method should have a static check_for_index function which checks for the existence of an index for the given reference in the given directories
	print "This class does not provide a check_for_index function, please use child classes instead!"

def build_index(reference, outputdir):
	# every annotation method should have a static build_index function which builds an annotation method specific index for the given reference in the given outputdir
	print "This class does not provide a build_index function, please use child classes instead!"