import os
import subprocess
import sys
import AnnotationMethod
import Utility
import shutil

bwa_path = "/usr/bin/bwa"

class Bwa(AnnotationMethod.AnnotationMethod):
    name = "Bwa"
    iterative = False

    def __init__(self, reads, reference, outputdir, tmp_dir, threads, parameters="", indexdirs=[], bg=False):
        AnnotationMethod.AnnotationMethod.__init__(self, reads, reference, outputdir, tmp_dir, threads, parameters, indexdirs, bg)

    def run(self):
        outputdir = self.outputdir+"/"+self.name  # each method should only produce output in a sub folder named like the method
        if not os.path.isdir(outputdir):
            os.makedirs(outputdir)

        shortref = Utility.short_file_name_without_extension(self.reference)
        sys.stdout.write("Mapping reads against "+shortref+"\n")

        # look for an existing index
        index = check_for_index(self.reference, [os.path.dirname(self.reference)]+self.indexdirs)

        # if index = None, build an index, otherwise use the found index
        if not index:
            sys.stdout.write("Building bwa index\n")
            index = build_index(self.reference, outputdir)
        else:
            sys.stdout.write("Found bwa index\n")

        # bwa mapping using several of the available cores
        sys.stdout.write("Starting bwa mapping\n")
        call = " ".join([bwa_path, "mem", self.parameters.strip(), index, self.reads, ">", outputdir+"/"+shortref+".sam"])
        sys.stdout.write("Invoking bwa with: "+call+"\n")
        subprocess.call(call, shell=True)

        # convert sam file to own file format smp [name sequence qualities (reference hitlength hitclippings hitmismatches)xN]
        sys.stdout.write("Splitting SAM file to SMP files\n")
        if not self.bg:
            (annotated_reads, unannotated_reads) = sam_to_smp_splitter(outputdir+"/"+shortref+".sam", outputdir)
        else:
            (annotated_reads, unannotated_reads) = sam_to_smp_splitter(outputdir+"/"+shortref+".sam", outputdir, give_unannotated=False)

        # clean up
        sys.stdout.write("Deleting redundant SAM file\n")
        os.remove(outputdir+"/"+shortref+".sam")

        # return paths to the files produced
        self.output = (annotated_reads, unannotated_reads)
        return self.output

def check_for_index(reference, list_of_dirs):
    # checks a given list of directories for existance of an index for the given reference, returns path to index if
    # any exists, otherwise returns None
    references = [Utility.short_file_name_without_extension(reference), os.path.basename(reference)]
    subfolders = ["Bwa", "."]

    for directory in list_of_dirs:
        for subfolder in subfolders:
            for ref in references:
                index = os.path.join(directory, subfolder, ref+".bwt")
                if os.path.isfile(index):
                    return os.path.splitext(index)[0]  # only return the prefix, not the bwt file name

def build_index(reference, outputdir):
    shortref = Utility.short_file_name_without_extension(reference)
    prefix = os.path.join(outputdir, shortref)
    call = " ".join([bwa_path, "index", reference])  # this creates indexfiles where the reference resides
    subprocess.call(call, shell=True)

    # this moves the created files where we want them
    extensions = [".amb", ".ann", ".bwt", ".pac", ".sa"]
    for extension in extensions:
        shutil.move(reference+extension, prefix+extension)

    return prefix

def sam_to_smp_splitter(inputfile, outputdir, give_unannotated=True):
    # convert sam file to own file format smp [name sequence qualities (reference hitlength hitclippings hitmismatches)xN]
    # expects all passed parameters to be string paths
    shortref = Utility.short_file_name_without_extension(inputfile)
    fsize = os.path.getsize(inputfile)
    bytesread = 0
    readlines = 0
    annotated_reads = open(outputdir+"/"+shortref+"_annotated_reads.smp", "w")
    if give_unannotated:
        unannotated_reads = open(outputdir+"/"+shortref+"_unannotated_reads.smp", "w")
    readname = ""
    samfile = open(outputdir+"/"+shortref+".sam")

    for line in samfile: # process line wise
        bytesread += len(line)+1
        readlines += 1
        if readlines % 100 == 0:
            sys.stdout.write("Processed " + str(100*bytesread/fsize) + "% of the file\r")
        if line[0]=="@": # ignore header lines
            continue
        else: # process other lines
            last_id = readname
            split = line.split("\t")
            readname = split[0]
            sequence = split[9]
            qualities = split[10]
            reference = split[2]
            if int(split[1])==4: # write unmapped sequences to their respective file / no hitlength or hitmismatches etc
                if give_unannotated:
                    unannotated_reads.write("\t".join([readname, sequence, qualities])+"\n")
            else: #write mapped sequences to their respective file / sequences matching more than once to the same line
                cigarlist = Utility.cigar_to_list(split[5]) # analyse cigar string for matches, mismatches and clippings
                (hitlength, hitmismatches, clippings) = Utility.evaluate_cigar_list(cigarlist)

                # search for bwas counter of mismatches ...
                for item in split:
                    if item.startswith("XM:i:"):
                        xm = int(item[5:])
                        hitlength -= xm
                        hitmismatches += xm
                        break

                if readname == last_id: # if same readname write NEW info to the same line / if this happens at all is dependent on the bwa output, usually it only gives the "best" alignment
                    annotated_reads.write("\t".join(["", reference, str(hitlength), str(clippings), str(hitmismatches)]))
                elif annotated_reads.tell() == 0: # if no sequence has been written to the output file so far
                    annotated_reads.write("\t".join([readname, sequence, qualities, reference, str(hitlength), str(clippings), str(hitmismatches)]))
                else:  # every other case, i.e. sequences matching the first time
                    annotated_reads.write("\n"+"\t".join([readname, sequence, qualities, reference, str(hitlength), str(clippings), str(hitmismatches)]))
    annotated_reads.write("\n")
    annotated_reads.close()
    if give_unannotated:
        unannotated_reads.close()
        return (os.path.abspath(annotated_reads.name), os.path.abspath(unannotated_reads.name))
    else:
        return (os.path.abspath(annotated_reads.name), "")
