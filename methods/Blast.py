import os
import subprocess
import sys
import threading
import AnnotationMethod
import Utility
import Stats
import gzip

class Blast(AnnotationMethod.AnnotationMethod):
	name = "Blast"

	def __init__(self, reads, reference, outputdir, tmp_dir, threads, parameters="", indexdirs=[], bg=False):
		AnnotationMethod.AnnotationMethod.__init__(self, reads, reference, outputdir, tmp_dir, threads, parameters, indexdirs, bg)

	def run(self, gz=False):
		outputdir = self.outputdir+"/"+self.name
		if not os.path.isdir(outputdir):
			os.makedirs(outputdir)

		shortref = Utility.short_file_name_without_extension(self.reference)
		sys.stdout.write("BLASTing reads against "+shortref+"\n")

		# look for an existing index
		index = check_for_index(self.reference, [os.path.dirname(self.reference)]+self.indexdirs)

		# if index = None, build an index, otherwise use the found index
		if not index:
			sys.stdout.write("Building BLAST index\n")
			index = build_index(self.reference, outputdir)
		else:
			sys.stdout.write("Found BLAST index\n")

		# reads file conversion to FASTA
		sys.stdout.write("Converting input to FASTA\n")
		shortreads = Utility.short_file_name_without_extension(self.reads)
		fastqreads = self.reads
		reads = outputdir+"/"+shortreads+".fasta"
		Utility.fastq_to_fasta_conversion(fastqreads, reads)

		sys.stdout.write("BLASTing\n")
		# if the user set parameters for blast contain -num_threads we won't combine that with our own threading!
		if not "-num_threads" in self.parameters:

			# threaded blast against the reference, variables
			cores = self.threads
			ram = Utility.calculate_usable_ram()
			chunk_size = (os.path.getsize(reads)/cores)/2 # since chunk_size for the files is not actually the size files will have, we use a safe approach here, halfing the size again
			max_chunk_size = (ram/cores)
			if chunk_size > max_chunk_size: # we want the chunks to be pretty small so every core gets something to do but we don't them to exceed our upper limit in any case (shouldn't really happen anyway)
				chunk_size = max_chunk_size
			chunk_list = Utility.split_fasta_file(reads, chunk_size, self.tmp_dir)
			semaphore = threading.BoundedSemaphore(cores)
			jobs = []
			counter = 0

			# threaded blast against the reference, actual blasting
			file_list = []
			for each in chunk_list:
				call = "blastn "+self.parameters.strip()+" -query "+each+" -db "+index+" -outfmt '6 qseqid sseqid qlen qstart qend qseq sseq' | gzip -c"
				#call = "blastn "+self.parameters.strip()+" -query "+each+" -db "+index+" -outfmt '6 qseqid sseqid length qlen mismatch gaps nident'"
				jobs.append(BLASTJob(each, index, self.tmp_dir+"/"+shortref+"_"+str(counter)+".blast.gz", semaphore, call))
				file_list += [self.tmp_dir+"/"+shortref+"_"+str(counter)+".blast.gz"] # building file list for later concatenation and deletion
				counter += 1

			for job in jobs:
				job.start()

			for job in jobs:
				job.join()

			# merging the output from threaded blasts
			Utility.concatenate_files(file_list, outputdir+"/"+shortref+".blast.gz") # should be sorted already since we start with a sorted file list

			# deleting temporary files
			for each in chunk_list:
				os.remove(each)
			for each in file_list:
				os.remove(each)

		# for use with -num_threads
		else:
			#if not self.bg:
			call = "blastn "+self.parameters.strip()+" -query "+reads+" -db "+index+" -outfmt '6 6 qseqid sseqid qlen qstart qend qseq sseq' | gzip -c > "+outputdir+"/"+shortref+".blast.gz"
			#call = "blastn "+self.parameters.strip()+" -query "+reads+" -db "+index+" -outfmt '6 qseqid sseqid length qlen mismatch gaps nident' > "+outputdir+"/"+shortref+".blast"
			#else:
			#	call = "blastn "+self.parameters.strip()+" -query "+reads+" -db "+index+" -outfmt '6 qseqid sseqid length qlen mismatch gaps' -num_alignments 1 > "+outputdir+"/"+shortref+".blast"
			sys.stdout.write("Invoking BLAST with: "+call+"\n")
			subprocess.call(call, shell=True)

		# at this point we have our output blast file outputdir+"/"+shortref+".blast"

		# create smps
		# unannotated (go through blast and fastq reads ... unannotated are all reads that don't appear in both files)
		sys.stdout.write("Creating unannotated SMP file\n")
		if not gz:
			unannotated_reads = open(outputdir+"/"+shortref+"_unannotated_reads.smp", "w")
		else:
			unannotated_reads = gzip.open(outputdir+"/"+shortref+"_unannotated_reads.smp.gz", "wt")

		gen = fastq_and_blast_data_merger(fastqreads, outputdir+"/"+shortref+".blast.gz", True)

		for read in gen:
			if not read[0]: # if the read appears only in the fastq file
				unannotated_reads.write("\t".join([read[1][0], read[1][1], read[1][2]+"\n"]))

		# annotated (go through blast file again and get all the best hits for every hit reference organism (only keeping the best hit on an organism if several of its references are hit))
		sys.stdout.write("Creating annotated SMP file\n")
		if not gz:
			annotated_reads = open(outputdir+"/"+shortref+"_annotated_reads.smp", "w")
		else:
			annotated_reads = gzip.open(outputdir+"/"+shortref+"_annotated_reads.smp.gz", "wt")
		blastgen = blast_hit_generator(outputdir+"/"+shortref+".blast.gz", True)
		fastqgen = Utility.read_generator_fastq_file(fastqreads)
		fastqread = next(fastqgen, False)

		# for every read ...
		for read in blastgen:
			readname = read[0]
			ref_dict = read[1]

			# ... get the corresponding fastq read for writing out the relevant stuff later on ...
			while fastqread[0] < readname:
				fastqread = next(fastqgen, False)

			# aaand we go through all the mappings and keep the best one for every organism hit (no cluttering, cause we only need the best hit on every organism)
			new_ref_dict = {}
			for ref_dict_key in ref_dict:
				read_length = int(ref_dict[ref_dict_key][0])
				qstart = int(ref_dict[ref_dict_key][1])
				qend = int(ref_dict[ref_dict_key][2])
				read_alignment = ref_dict[ref_dict_key][3]
				ref_alignment = ref_dict[ref_dict_key][4]

				#aligment_length = len(read_alignment)
				hit_mismatches = 0
				hit_matches = 0
				hit_read_gaps = 0
				hit_ref_gaps = 0
				hit_clippings = (qstart-1)+(read_length-qend)  # left clips + right clips

				for index, character in enumerate(read_alignment):
					if read_alignment[index] == ref_alignment[index]:  # sequence match
						hit_matches += 1
					elif read_alignment[index] == "-":  # gap in read / deletion from reference
						hit_read_gaps += 1
					elif ref_alignment[index] == "-":  # gap in reference / insertion to reference
						hit_ref_gaps += 1
					else:  # normal mismatch
						hit_mismatches += 1

				# read gaps aren't in the initial read length and can cause the sum of numbers to add up differently for different hits by the same read, thus screwing up the score calculation and comparison
				hit_mismatches += hit_ref_gaps

				lowest_exact_taxonomy = ref_dict_key.split("|")[1]
				if not lowest_exact_taxonomy in new_ref_dict:
					new_ref_dict[lowest_exact_taxonomy] = [ref_dict_key, hit_matches, hit_clippings, hit_mismatches]
				else: # need to compare if this hit is better ... if it's only the same or worse, don't consider it, if better replace the current one
					score_in_dict = Stats.read_scoring_system(read_length, new_ref_dict[lowest_exact_taxonomy][1], new_ref_dict[lowest_exact_taxonomy][2], new_ref_dict[lowest_exact_taxonomy][3])
					this_score = Stats.read_scoring_system(read_length, hit_matches, hit_clippings, hit_mismatches)
					if this_score > score_in_dict:
						new_ref_dict[lowest_exact_taxonomy] = [ref_dict_key, hit_matches, hit_clippings, hit_mismatches]

			# make a sorted list out of the dict
			hit_ref_list = []
			for new_ref_dict_key in new_ref_dict:
				hit_ref_list += [new_ref_dict[new_ref_dict_key]]
			hit_ref_list = sorted(hit_ref_list, key=lambda x: int(x[0].split("|")[1])) # this sorts by the gi number of each reference / changed to make it work with newest db version 04/05/16

			# write out list
			annotated_reads.write("\t".join([readname, fastqread[1], fastqread[3]]))
			for hit_ref in hit_ref_list:
				annotated_reads.write("\t")
				annotated_reads.write("\t".join([str(x) for x in hit_ref]))
			annotated_reads.write("\n")

		# clean up blast file
		os.remove(outputdir+"/"+shortref+".blast.gz")

		# return paths to smp files
		self.output = (os.path.abspath(annotated_reads.name), os.path.abspath(unannotated_reads.name))
		return self.output

class BLASTJob(threading.Thread):
	def __init__(self, reads, reference, outputfile, semaphore, call):
		# reads is expected to be a string path to a fasta file containing the reads to blast
		# reference is expected to be a string to a db in short format e.g. /somewhere/somewhere_else/database (omitting the file endings of the db files)
		# outputfile is expected to be a string path to the output file (e.g. /path/to/results.blast)
		threading.Thread.__init__(self)
		self.reads = reads
		self.reference = reference
		self.outputfile = outputfile
		self.semaphore = semaphore
		self.call = call

	def run(self):
		self.semaphore.acquire()
		sys.stdout.write("Invoking BLAST with: "+self.call+"\n")
		subprocess.call(self.call+" > "+self.outputfile, shell=True)
		sys.stdout.write("Finished BLASTing "+self.reads+"\n")
		self.semaphore.release()

def check_for_index(reference, list_of_dirs):
	# checks a given list of directories for existance of an index for the given reference, returns path to index if
	# any exists, otherwise returns None
	references = [Utility.short_file_name_without_extension(reference), os.path.basename(reference)]
	subfolders = ["BLAST", "."]

	for directory in list_of_dirs:
		for subfolder in subfolders:
			for ref in references:
				if os.path.isfile(directory+"/"+subfolder+"/"+ref+".nsq") or os.path.isfile(directory+"/"+subfolder+"/"+ref+".nal"):
					return directory+"/"+subfolder+"/"+ref

def build_index(reference, outputdir):
	shortref = Utility.short_file_name_without_extension(reference)
	subprocess.call("makeblastdb -dbtype nucl -in "+reference+" -title "+shortref+" -out "+outputdir+"/"+shortref, shell=True)
	return os.path.abspath(outputdir)+"/"+shortref

def blast_hit_generator(blastfile, gz=False):
	lastname = None
	chunk = [None, {}] # chunk[1] is a dictionary of references hit as the keys and
	if not gz:
		infile = open(blastfile)
	else:
		infile = gzip.open(blastfile, 'rt')
	for line in infile:
		line_split = line.rstrip().split()
		readname = line_split[0]
		reference = line_split[1]
		hit_values = line_split[2:]
		if readname == lastname:
			chunk[1][reference] = hit_values
		else:
			if not chunk[0] is None:
				yield chunk
			lastname = readname
			chunk = [readname, {}]
			chunk[1][reference] = hit_values

	# yield last chunk
	if not chunk[0] is None:
		yield chunk

def fastq_and_blast_data_merger(fastqfile, blastfile, blast_gz=False):
	"""
	A generator that goes through SORTED fastq and blast result files with corresponding reads and returns a tuple
	(<Boolean designating if the read appears in both files (False == Only in the fastq file)>, [Name, Sequence, Qualities, Hit References {Name, Hit Values}])

	:param fastqfile: string path to a fastq file
	:param blastfile: string path to a blast file with reads that constitute a subset of those in the fastqfile
	:return: a tuple of (<Bool>, [Name, Sequence, Qualities, Hit References {Name, Hit Values}])
	"""

	fastqgen = Utility.read_generator_fastq_file(fastqfile)
	blastgen = blast_hit_generator(blastfile, blast_gz)

	fastqread = next(fastqgen, None)
	blastread = next(blastgen, None)

	while fastqread: # since the blast reads are always a subset of the fastq reads, we iterate over the generator with more elements
		if blastread: # while we still have reads in the blast file
			if fastqread[0] == blastread[0]: # if both names are the same, we report everything together and get the next read from both files
				yield (True, [fastqread[0], fastqread[1], fastqread[3], blastread[1]])
				fastqread = next(fastqgen, None)
				blastread = next(blastgen, None)
			elif fastqread[0] < blastread[0]: # if the fastq read is smaller, which can happen since blast reads might be a strict subset of the fastq reads
				yield (False, [fastqread[0], fastqread[1], fastqread[3], {}])
				fastqread = next(fastqgen, None)
			else: # blast reads should never be smaller than fastq reads in the case of sorted files
				raise ValueError, "Blast Read "+blastread[0]+" < FastQ Read "+fastqread[0]
		else: # no reads left in the blast file so we just report every left fastq with the False flag and an empty hit dictionary
			yield (False, [fastqread[0], fastqread[1], fastqread[3], {}])
			fastqread = next(fastqgen, None)
