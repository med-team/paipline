# Pipeline for the Automatic Identification of Pathogens (PAIPline)

## Description

A Python program designed to search for pathogen nucleic acid sequences in NGS datasets.
This program needs databases in the format provided by the database-updater found under https://gitlab.com/andreas.andrusch/database-updater

## LICENSE

This program is released under the GNU GENERAL PUBLIC LICENSE Version 3 as outlined in http://www.gnu.org/licenses/gpl-3.0.txt or in the LICENSE.txt included in this project.

