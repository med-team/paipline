import os
import pickle

ranks_lookup = ['superkingdom', 'kingdom', 'subkingdom', 'superphylum', 'phylum', 'subphylum', 'superclass', 'class',
                'subclass', 'infraclass', 'superorder', 'order', 'suborder', 'infraorder', 'parvorder', 'superfamily',
                'family', 'subfamily', 'infrafamily', 'tribe', 'subtribe', 'infratribe', 'genus', 'subgenus',
                'species_group', 'species_subgroup', 'species', 'subspecies', 'varietas', 'forma']

class Node:
    def __init__(self, level, taxid, name):
        self.level = level
        self.taxid = taxid
        self.name = name
        self.children = {}
        self.parent = None
        self.hits_below = [0, 0, 0] # all hits, unique hits, unambiguous hits
        self.hits_specific = [0, 0, 0] # all hits, unique hits, unambiguous hits
        self.marker = "" # empty string is unmarked
        self.score = 0.0
        self.unambiguous = False # initialize as False and set to True if entry is unambiguous at this level
        self.unambiguous_reads = set()
        self.hits_below_dict = {}
        self.hits_specific_dict = {}
        self.set_of_reads = set()

    def add_child(self, child):
        if not self.children.has_key(child.name):
            child.parent = self
            self.children[child.name] = child
        return self.children[child.name]

    def add_children(self, defline):
        if len(defline) == 0:
            return self #return the last node in the path / the leaf upwards through the recursion
        next_child_split = defline[-1].split(":")
        child = self.add_child(Node(next_child_split[0], next_child_split[1], next_child_split[2]))
        return child.add_children(defline[:-1])

    def build_read_tree(self, defline):
        # this method sets hit values to one and is inappropriate to add reads to existing trees but only to create new read trees
        if len(defline) == 0: # leaf node
            self.hits_specific[0] = 1
            return self # return the leaf
        else: # branch node
            self.hits_below[0] = 1
            next_child_split = defline[-1].split(":")
            try:
                child = self.add_child(Node(next_child_split[0], next_child_split[1], next_child_split[2]))
                return child.build_read_tree(defline[:-1])
            except IndexError:  # this is here for databases that are not fully compliant to our scheme :(
                self.hits_specific[0] = 1
                return self  # return the leaf

    def has_child(self, defline):
        lastname = defline[-1].split(":")[2]
        if self.children.has_key(lastname):
            if len(defline) == 1:
                return self.children[lastname] # return child instead of True in case we need this child object, bool checks will still work
            else:
                return self.children[lastname].has_child(defline[:-1])
        else:
            return None

    def get_sum_of_hits(self):
        return [self.hits_below[0]+self.hits_specific[0], self.hits_below[1]+self.hits_specific[1], self.hits_below[2]+self.hits_specific[2]]

    def get_sum_of_hits_for_sample(self, sample):
        return [self.hits_below_dict[sample][0]+self.hits_specific_dict[sample][0], self.hits_below_dict[sample][1]+self.hits_specific_dict[sample][1], self.hits_below_dict[sample][2]+self.hits_specific_dict[sample][2]]

    def unambiguous_fastas_from_tree(self, outputfolder, amount=0):
        # amount = 0 will write out all reads
        # write out fastas with unambiguous reads for this node
        counter = 0
        if self.unambiguous_reads:
            with open(outputfolder+"/"+self.name+".fasta", "w") as outfile:
                for read in self.unambiguous_reads:
                    counter += 1
                    outfile.write("".join([">",read.name,"\n",read.sequence,"\n"]))
                    if counter == amount:
                        break
        # process child nodes
        for child in self.children:
            self.children[child].unambiguous_fastas_from_tree(outputfolder, amount)

    def unambiguous_fastas_from_tree_including_lower_levels(self, outputfolder, amount=0):
        # amount = 0 will write out all reads
        # write out fastas with unambiguous reads for this node and all nodes below
        # get reads from all children and this level
        sec_reads = self.get_unambiguous_reads_from_this_node_and_below()
        # build fasta for this node
        counter = 0
        if sec_reads:
            with open(outputfolder+"/"+self.name+".fasta", "w") as outfile:
                for read in sec_reads:
                    counter += 1
                    outfile.write("".join([">",read.name,"\n",read.sequence,"\n"]))
                    if counter == amount:
                        break
        # process child nodes
        for child in self.children:
            self.children[child].unambiguous_fastas_from_tree_including_lower_levels(outputfolder, amount)

    def get_unambiguous_reads_from_this_node_and_below(self):
        #ret_reads = []
        ret_reads = set() | self.unambiguous_reads # deep copy
        for child in self.children:
            ret_reads = ret_reads | self.children[child].get_unambiguous_reads_from_this_node_and_below()
        return ret_reads

    def add_upwards_to_unambiguous(self):
        self.hits_below[2] += 1
        if self.parent:
            self.parent.add_upwards_to_unambiguous()

    def add_upwards_to_unique(self):
        self.hits_below[1] += 1
        if self.parent:
            self.parent.add_upwards_to_unique()

    def remove_ambiguous_nodes(self):
        # trim ambiguous nodes from a tree
        # remove all ambiguous children of this node
        # this should leave an unary tree / a list of nodes
        helper_list = []
        for child in self.children:
            if not self.children[child].unambiguous:
                helper_list += [child]
        for child in helper_list:
            del self.children[child]
        # recurse through the tree
        if self.children:
            for child in self.children:
                return self.children[child].remove_ambiguous_nodes()
        else:
            return self

    def construct_defline(self, defline="0"):
        """
        A function that gives back a defline describing the taxonomic path to this node

        :param defline: String defline to start from (usually a GI or a 0 as substitute)
        :return: String defline
        """

        if not self.parent:  # root node, so we just return what we have so far
            return defline
        else:  # every other node, so we add to our list and recurse through the tree upwards
            defline = "|".join([defline, ":".join([self.level, self.taxid, self.name])])
            return self.parent.construct_defline(defline)


def walk_tree_and_create_name_dictionary(node, dictionary):
    try:
        dictionary[node.name] += [node]
    except KeyError:
        dictionary[node.name] = [node]
    for child in node.children.values():
        walk_tree_and_create_name_dictionary(child, dictionary)

def mark_branch_downwards(node, marker):
    node.marker = marker
    for child in node.children.values():
        mark_branch_downwards(child, marker)

def mark_branch_upwards(node, marker):
    node.marker = marker
    # check if all children of parent are marked, if not, we don't mark the parent, if yes, we continue upwards
    for sibling in node.parent.children.values():
        if sibling.marker != marker:
            return
    mark_branch_upwards(node.parent, marker)

def add_all_hits(node, defline):
    lastname = defline[-1].split(":")[2]
    if lastname in node.children:
        if len(defline) > 1: # more than one child
            node.hits_below[0] += 1 # add for below on this level
            return add_all_hits(node.children[lastname], defline[:-1])
        else: # last child
            node.hits_below[0] += 1
            node.children[lastname].hits_specific[0] += 1
            return node

def add_unique_hits(node, defline):
    lastname = defline[-1].split(":")[2]
    if lastname in node.children:
        if len(defline) > 1: # more than one child
            node.hits_below[1] += 1 # add for below on this level
            return add_unique_hits(node.children[lastname], defline[:-1])
        else: # last child
            node.hits_below[1] += 1
            node.children[lastname].hits_specific[1] += 1
            return node

def add_unambiguous_hits(node, defline):
    lastname = defline[-1].split(":")[2]
    if lastname in node.children:
        if len(defline) > 1: # more than one child
            node.hits_below[2] += 1 # add for below on this level
            return add_unambiguous_hits(node.children[lastname], defline[:-1])
        else: # last child
            node.hits_below[2] += 1
            node.children[lastname].hits_specific[2] += 1
            return node

def print_tree(node, indent=""):
    #print "".join([indent, node.level, " : ", node.name, " - ", str(node.hits_specific), " ", str(node.hits_below), " ", str(node.get_sum_of_hits()), " + ", str(node.score), " ", str(node.unambiguous)])
    #print "".join([indent, node.level, " : ", node.name, " - ", str(node.score), " ", str(node.unambiguous), " ", str(node.set_of_reads)])
    if not node.hits_below_dict:
        print "".join([indent, node.level, " : ", node.name, " - ", str(node.hits_specific), str(node.hits_below)])
    else:
        print "".join([indent, node.level, " : ", node.name, " - ", str(node.hits_specific_dict), str(node.hits_below_dict)])
    for child in node.children.values():
        print_tree(child, indent + "\t")

def add_output(node, structure, output_list, counter=0, sort_by=0):
    if node.level == "no_rank":
        counter += 1
    else:
        counter = structure.index(node.level)
    sums = node.get_sum_of_hits()
    # a line with the marker at the front, indents, name for the current tax level, more indents and the numbers of reads
    line_string = ";".join([node.marker]+counter*[""]+[node.name]+(len(structure)-counter-1)*[""]+[node.name+" ("+node.level+")"]+[str(sums[0]), str(sums[1]), str(sums[2])+"\n"])
    output_list += [line_string]
    for childname, childnode in sorted(node.children.items(), key=lambda x: x[1].get_sum_of_hits()[sort_by], reverse=True): # sorting children by the sum of total hits
        add_output(childnode, structure, output_list, counter, sort_by)

def full_output(node, structure, output_list, sort_by=0, output_nodes_without_unambiguous_reads=False):
    # get defline for this node
    list_of_nodes = []
    defline = construct_path_for_node(node, list_of_nodes)
    list_of_nodes = [["root", "root"]]+[[x.split(":")[2], x.split(":")[0]] for x in defline.split("|") if ":" in x][::-1]
    # list_of_nodes contains our path now, from root as first element to this node as last element
    ordered = []
    for name, level in list_of_nodes:
        if level == "no_rank" or level == "root":
            ordered += [name]
        else:
            index = structure.index(level)
            diff = index - len(ordered) # current len - len it should have just before the addition of the last taxonomy
            ordered += diff * [""] # adding space before the next taxonomy
            ordered += [name]
    #print ordered
    sums = node.get_sum_of_hits()
    if output_nodes_without_unambiguous_reads or (sums[2] > 0):
        line_string = ";".join([node.marker]+ordered+(len(structure)-len(ordered))*[""]+[node.name+" ("+node.level+")"]+[str(sums[0]), str(sums[1]), str(sums[2]), str(node.hits_below[2]), str(node.hits_specific[2])+"\n"])
        output_list += [line_string]
    for childname, childnode in sorted(node.children.items(), key=lambda x: x[1].get_sum_of_hits()[sort_by], reverse=True): # sorting children by the sum of total hits
        full_output(childnode, structure, output_list, sort_by, output_nodes_without_unambiguous_reads)

def count_indents(node, dictionary, lastlevel="root", no_ranks=0):
    if node.level != "no_rank":
        lastlevel = node.level
        no_ranks = 0
        try:
            if dictionary[lastlevel] < no_ranks:
                dictionary[lastlevel] = no_ranks
        except KeyError:
            dictionary[lastlevel] = no_ranks
    else:
        no_ranks += 1
        try:
            if dictionary[lastlevel] < no_ranks:
                dictionary[lastlevel] = no_ranks
        except KeyError:
            dictionary[lastlevel] = no_ranks
    for child in node.children.values():
        count_indents(child, dictionary, lastlevel, no_ranks)

def construct_stats_file_structure(dictionary, lookup):
    res = []
    for each in lookup:
        if each in dictionary:
            res += [each] + ["no_rank"]*dictionary[each]
    return res

def give_score(node, ambiguity_threshold=0.10):
    if not node.children: # last node in a path / a leaf
        return (node.score, node)
    else: # any other node
        list_of_scores = []
        for child in node.children:
            list_of_scores += [give_score(node.children[child], ambiguity_threshold)]
        list_of_scores.sort(reverse=True)
        node.score = list_of_scores[0][0] # set this node's score to highest score of children
        if len(list_of_scores) == 1 or list_of_scores[0][0]/list_of_scores[1][0] >= (1.0+ambiguity_threshold): # either unique or unambiguous
            list_of_scores[0][1].unambiguous = True
        return (node.score, node)

def give_scores(node, ambiguity_dict, use_dict="all"):
    if not node.children: # last node in a path / a leaf
        return (node.score, node)
    else: # any other node
        list_of_scores = []
        if node.name in ambiguity_dict:
            use_dict = node.name
        for child in node.children:
            list_of_scores += [give_scores(node.children[child], ambiguity_dict, use_dict)]
        list_of_scores.sort(reverse=True)
        node.score = list_of_scores[0][0] # set this node's score to highest score of children
        if node.level in ranks_lookup:
            level = node.level
        else:
            level = give_next_upwards_fixed_taxonomic_rank(node)
        try:
            if len(list_of_scores) == 1 or list_of_scores[0][0]/list_of_scores[1][0] >= (1.0+ambiguity_dict[use_dict][level]): # either unique or unambiguous
                list_of_scores[0][1].unambiguous = True
        except KeyError:
            if len(list_of_scores) == 1 or list_of_scores[0][0]/list_of_scores[1][0] >= (1.0+ambiguity_dict[use_dict]["all"]): # either unique or unambiguous
                list_of_scores[0][1].unambiguous = True
        return (node.score, node)

def give_next_upwards_fixed_taxonomic_rank(node):
        if node.level in ranks_lookup:
            return node.level
        elif node.parent:
            return give_next_upwards_fixed_taxonomic_rank(node.parent)
        else:
            return ranks_lookup[0] # if no parent with a fixed level, we return the highest level in our lookup table

def get_unambiguous_path(node):
    unambiguous_child = None
    for child in node.children:
        if node.children[child].unambiguous:
            unambiguous_child = node.children[child]
            break
    if unambiguous_child:
        return get_unambiguous_path(unambiguous_child)
    else:
        return node

def construct_path_for_node(node, list_of_nodes=[]):
    list_of_nodes += [[node.name, node.taxid, node.level]]
    if node.parent:
        return construct_path_for_node(node.parent, list_of_nodes)
    else:
        # join the entries to our standard defline
        del list_of_nodes[-1] # this is the root entry, which we don't want in our defline
        for index, node_desc in enumerate(list_of_nodes):
            list_of_nodes[index] = ":".join([list_of_nodes[index][2], str(list_of_nodes[index][1]), list_of_nodes[index][0]])
        defline = "|".join(list_of_nodes)
        return defline

def add_unambiguous_count_from_read_tree(sum_tree_root_node, read_hits_tree_root_node):
    unambiguous_node = get_unambiguous_path(read_hits_tree_root_node)
    list_of_nodes = []
    unambiguous_path = construct_path_for_node(unambiguous_node, list_of_nodes)
    if unambiguous_path:
        split = unambiguous_path.split("|")
        sum_tree_unambiguous_node = sum_tree_root_node.has_child(split)
        add_unambiguous_hits_upwards(sum_tree_unambiguous_node)
        return sum_tree_unambiguous_node

def add_unambiguous_hits_upwards(node, first=True):
    if first:
        node.hits_specific = [node.hits_specific[0], node.hits_specific[1], node.hits_specific[2]+1]
    else:
        node.hits_below = [node.hits_below[0], node.hits_below[1], node.hits_below[2]+1]
    if node.parent:
        add_unambiguous_hits_upwards(node.parent, False)

def add_nodes_with_hit_data_to_merge_tree(node, merge_tree, samplename):
    if node.parent: # don't add the roots to root, that would be silly, although hilarious
        defline = construct_path_for_node(node, [])
        merged_node = merge_tree.add_children(defline.split("|"))
        merged_node.hits_below_dict[samplename] = node.hits_below
        merged_node.hits_specific_dict[samplename] = node.hits_specific
    for child in node.children:
        add_nodes_with_hit_data_to_merge_tree(node.children[child], merge_tree, samplename)

def full_output_for_merged_tree(node, structure, list_of_samples, output_list, sort_by=0, output_nodes_without_unambiguous_nodes=False):
    # get defline for this node
    list_of_nodes = []
    defline = construct_path_for_node(node, list_of_nodes)
    list_of_nodes = [["root", "root"]]+[[x.split(":")[2], x.split(":")[0]] for x in defline.split("|") if ":" in x][::-1]
    # list_of_nodes contains our path now, from root as first element to this node as last element
    ordered = []
    for name, level in list_of_nodes:
        if level == "no_rank" or level == "root":
            ordered += [name]
        else:
            index = structure.index(level)
            diff = index - len(ordered) # current len - len it should have just before the addition of the last taxonomy
            ordered += diff * [""] # adding space before the next taxonomy
            ordered += [name]

    # do actual stuff here
    # check if any sample has unambiguous reads for this node or below
    unambiguous = False
    for sample in list_of_samples:
        if node.get_sum_of_hits_for_sample(sample)[2] > 0:
            unambiguous = True
            break
    if output_nodes_without_unambiguous_nodes or unambiguous:
        # construct line with taxonomy
        line = [node.marker]+ordered+(len(structure)-len(ordered))*[""]+[node.name+" ("+node.level+")"]
        # add all hits for samples in a sorted fashion
        for sample in list_of_samples:
            #line += node.get_sum_of_hits_for_sample(sample)  # this gives sums of hits below and specific
            line += [node.hits_specific_dict[sample][2]] # this gives only the specific unambiguous_hits
        output_list += [";".join([str(x) for x in line])+"\n"]

    for childname, childnode in sorted(node.children.items(), key=lambda x: x[1].get_sum_of_hits()[sort_by], reverse=True): # sorting children by the sum of total hits
        full_output_for_merged_tree(childnode, structure, list_of_samples, output_list, sort_by, output_nodes_without_unambiguous_nodes)

def create_csv_from_several_samples(list_with_treeroots_and_samplenames, outputfile, neg_list=None):
    list_with_treeroots_and_samplenames = sorted(list_with_treeroots_and_samplenames, key=lambda x: x[1])
    list_of_samples = [x[1] for x in list_with_treeroots_and_samplenames]
    treeroot = Node("root", "0", "root")
    for node, name in list_with_treeroots_and_samplenames:
        try:
            add_nodes_with_hit_data_to_merge_tree(node, treeroot, name)
        except AttributeError:
            print name, "dump file too old to merge properly"

    indents_dict = {}
    count_indents(treeroot, indents_dict)
    columns = construct_stats_file_structure(indents_dict, ranks_lookup)
    columns = ["root"] + columns  # root needs a column too in the output file

    zero_fill_merge_tree(treeroot, list_of_samples)

    if neg_list:
        # create dictionary of all nodes with names by walking the tree once
        dict_of_all_nodes = {}
        walk_tree_and_create_name_dictionary(treeroot, dict_of_all_nodes)

        # create list of uninteresting taxa from file
        uninteresting_list = []
        if neg_list:
            with open(neg_list) as infile:
                for line in infile:
                    uninteresting_list += [line.rstrip().split()[0]]

        # mark entries in tree if they are in the uninteresting list, downwards
        for entry in uninteresting_list:
            if entry in dict_of_all_nodes:
                for node in dict_of_all_nodes[entry]:
                    mark_branch_downwards(node, marker="-") # - for unintersting

        # now that we can say if a node's children are ALL marked, we mark upwards
        for entry in uninteresting_list:
            if entry in dict_of_all_nodes:
                for node in dict_of_all_nodes[entry]:
                    mark_branch_upwards(node, marker="-") # - for unintersting

    outlist = []
    full_output_for_merged_tree(treeroot, columns, list_of_samples, outlist)
    sample_structure_list = []
    for sample in list_of_samples:
        #sample_structure_list += [sample, "", ""]
        sample_structure_list += [sample]
    with open(outputfile, "w") as outfile:
        # really dirty header construction
        header2 = ["marker"]+columns+["hit_name"]
        header1 = len(header2)*[""]+sample_structure_list+["\n"]
        #header2 += len(sample_structure_list)/5*["all_hits", "unique_hits", "unambiguous_hits"]+["\n"]
        header2 += len(sample_structure_list)*["unambiguous_hits_specific"]+["\n"]
        outfile.write(";".join(header1))
        outfile.write(";".join(header2))
        for line in outlist:
            outfile.write(line)

def create_sample_name_and_tree_object_list(folders):
    samples = [] # [n x [Node object / tree root, sample name]]
    temp = []

    for folder in folders:
        temp += [os.path.join(folder, x) for x in os.listdir(folder) if os.path.isdir(os.path.join(folder, x)) and os.path.isfile(os.path.join(folder, x, "final_output", "treedump.pickle"))]

    temp = sorted(temp)

    for index, samplepath in enumerate(temp):
        try:
            samples += [[pickle.load(open(os.path.join(temp[index], "final_output", "treedump.pickle"))), os.path.basename(temp[index])]]
        except EOFError:
            print os.path.basename(temp[index]), "not readable :("

    return samples

def zero_fill_merge_tree(node, list_of_samples):
    for sample in list_of_samples:
        if not sample in node.hits_specific_dict:
            node.hits_specific_dict[sample] = [0, 0, 0]
        if not sample in node.hits_below_dict:
            node.hits_below_dict[sample] = [0, 0, 0]
    for child in node.children:
        zero_fill_merge_tree(node.children[child], list_of_samples)

def merge_read_tree_into_sum_tree(readroot, sumroot):
    # starting with both roots
    current_sum_node = sumroot
    current_read_node = readroot
    last_sum_node = current_sum_node

    # go through the nodes
    while current_read_node:
        # add values if node exists in both trees
        if current_sum_node and current_sum_node.name == current_read_node.name:
            for i in range(3):
                current_sum_node.hits_specific[i] += current_read_node.hits_specific[i]
                current_sum_node.hits_below[i] += current_read_node.hits_below[i]
            current_sum_node.set_of_reads = current_sum_node.set_of_reads | current_read_node.set_of_reads
            current_sum_node.unambiguous_reads = current_sum_node.unambiguous_reads | current_read_node.unambiguous_reads
        # add remaining read tree if node doesnt exist in sum tree, break in this case
        else:
            last_sum_node.children[current_read_node.name] = current_read_node
            current_read_node.parent = last_sum_node
            break
        # otherwise go down one step in both trees
        current_read_node = go_to_only_child_in_read_tree(current_read_node)
        if current_read_node:
            last_sum_node = current_sum_node
            current_sum_node = select_child_by_name(current_sum_node, current_read_node.name)

def select_child_by_name(node, name):
    if name in node.children:
        return node.children[name]
    return None

def go_to_only_child_in_read_tree(node):
    # go to the next child in the read tree
    assert len(node.children) in [0, 1]  # just to test if the tree is unary
    for child in node.children:
        current_read_node = node.children[child]
        break
    else:
        current_read_node = None
    return current_read_node

def main():
    pass

if __name__ == "__main__":
    main()