#!/usr/bin/env python

import argparse
import os
import pickle
import shlex
import sys
import uuid
import Project
import Utility

def main(): # Main routine of the program, processing command line arguments, making tests required for safe execution of the program and finally creating or loading the project itself

	# help flag
	project_loaded = False

	# Parsing command line arguments
	parser = argparse.ArgumentParser(description="Pipeline for the Automatic Identification of Pathogens (PAIPline)")
	parser.add_argument("-i", "--inputfile", help="Should be a path to the fastq input file; if omitted, the output directory will be checked for a loadable project")
	parser.add_argument("-o", "--outputdir", help="Should be a path to the output directory to which all data will be written, this is also checked for loadable project files")
	parser.add_argument("-C", "--configfile", help="Can be a path to a config file, defining the parameters to run the program with, takes precedence over potentially given command line arguments, although databases given on the commandline will be used along with the ones given in the config file; if omitted, will use command line given parameters only")
	parser.add_argument("-d", "--database", help="Should be a path to a fasta file to be used as a database for pathogen search, can be either a fasta file or a directory containing fasta files; can be used multiple times, the program will work through all given databases sequentially", default=[], action="append")
	parser.add_argument("-b", "--background", help="Should be a path to a fasta file to be used as a database for background removal, can be either a fasta file or a directory containing fasta files; can be used multiple times, the program will work through all given databases sequentially", default=[], action="append")
	parser.add_argument("-m", "--method", help="Adds an annotation method to the pipeline, can be used multiple times, methods will then be worked through consecutively", default=[], action="append")
	parser.add_argument("-a", "--arguments", help="Can be used to specify arguments for a method if it should not be run with the built-in standard parameters, can be used multiple times. Note that handling the input and output files is always done by the program. Values need to be formatted like this '<method name>:<bg/fg>:<parameters>', for example 'Bowtie2:fg:--very-sensitive-local -p 40 --no-hd' and 'Bowtie2:bg:--very-fast -p 24'. bg and fg define if the parameters will be used for background or foreground analyses respectively.", default=[], action="append")
	parser.add_argument("-p", "--preindexed", help="Can be used to specify a folder containing pre-indexed databases that could be of use for any of the used annotation methods, i.e. BLAST databases or Bowtie2 indices, can be used multiple times", default=[], action="append")
	parser.add_argument("-g", "--group", help="Can be a string by which results will be grouped when statistics output is created. Typical values are species, genus, family, order, class.", default="")
	parser.add_argument("-G", "--grouped_only", help="Can be used to omit all non-groupable hits from the reports (also skipping their processing during the iterative program part, thus speeding up the program, especially if it would be necessary to build indices otherwise). This is ignored unless -g is also given.", action="store_true")
	parser.add_argument("-P", "--preprocessing", help="Can be used to preprocess the input file on a basic level to ensure removal of short reads and trimming of bad quality reads. It is HIGHLY encouraged to preprocess your input files with a third-party program to your liking.", action="store_true")
	parser.add_argument("-I", "--internal-control", help="Can be a path to a fastq file that will be used as a control, meaning that its reads will be added to the input file and marked as control. Note that these reads are not automatically evaluated.")
	parser.add_argument("-q", "--quiet", help="Can be used to suppress questions before starting a project if enough options are given. Using this option will cause no questions for the user to be raised. It will always load an already existant project in the desired output folder, if any. Otherwise a new project will be started.", action="store_true")
	parser.add_argument("-t", "--threads", help="Can be used to set the number of processes to use with any subprocesses to this value, e.g. the number of available processors.; Default: 50 percent of available cores", default=0, type=int)
	parser.add_argument("-n", "--nt", help="A parameter giving the path to a directory with fasta files to be used during the iterative part of Bowtie2 analyses for eliminating false positives; usually the whole nt database (possibly in chunks); if this parameter is not given, the BLAST validation is not run.")
	parser.add_argument("-A", "--ambiguity", help="A parameter defining the necessary percental difference for the two best hits for a read, so that that read can be classified as unambigous; higher value means more specificity but less sensitivity; formatted as <taxonomic level>:<percentage> or <taxonomy>:<taxonomic level>:<percentage> if the cut-off should be used for a specific taxonomy and below only; Can be used several times for different levels, if a level is given more than once, the last setting will be used; Default: <all levels>:10", default=["all:10"], action="append")
	#parser.add_argument("-l", "--ambiguity_level", help="A parameter defining the level on which hits of reads should be considered too closely related to be compared with the current ambiguity level threshold; Default: family", default="family")
	parser.add_argument("-u", "--uninteresting_list", help="An optional parameter giving a path to a text file listing potentially uninteresting taxa to be marked in the final stats for filtering; Default none", default="")
	parser.add_argument("-T", "--tempdir", help="A parameter giving the path to a directory that can be used as a temporary directory for file operations such as sorting; Default: /tmp/<uuid>", default="/tmp/<uuid>")
	parser.add_argument("-r", "--reevaluate", help="If this parameter is given, the project will not be run but only re-evaluated with the given parameters (needs to be used in combination with -o, so that an already run project can be loaded)", default=False, action="store_true")
	parser.add_argument("-O", "--only_unambiguous", help="This parameter determines if only references that were hit unambiguously are reported or all hit references; Default: True", default=True, action="store_false")
	# todo parameter for number of reads that should be used during blast validation if threshold is exceeded / reporting of the ratio of reads in stats?!
	# todo parameter to create new stats for a project with the chosen parameters only
	args_commandline = parser.parse_args(sys.argv[1:])

	# if config file is provided parse the parameters from that as well
	if (args_commandline.configfile is not None) and os.path.isfile(args_commandline.configfile):
		args_configfile = parser.parse_args(get_parameters_from_config_file(args_commandline.configfile))
		args = combine_input(args_configfile, args_commandline)
	else: # file doesn't exist
		sys.stdout.write("No config file specified or given config file does not exist, using input from command line only.\n")
		args = args_commandline.__dict__

	# processing some cli parameters
	if args["outputdir"] and args["outputdir"].endswith("/"):
		args["outputdir"] = args["outputdir"][:-1]
	if args["tempdir"] == "/tmp/<uuid>":
		args["tempdir"] = "/tmp/"+str(uuid.uuid4())
	else:
		if args["tempdir"].endswith("/"):
			args["tempdir"] += str(uuid.uuid4())
		else:
			args["tempdir"] += "/"+str(uuid.uuid4())
	if args["threads"] == 0:
		args["threads"] = Utility.calculate_optimal_cores_to_use()

	# check ambiguity settings
	args["ambiguity"] = process_ambiguity_settings(args["ambiguity"])

	# create tmp dir if necessary / moved to the point where a new project is created so we don't clutter up the tmp dir with dirs that aren't used when loading projects

	# check through all the parameters given individually for validity and then check if runable combinations are provided

	# check if output dir is even specified
	if args["outputdir"] is None:
		sys.stdout.write("No output directory specified.\n")
		sys.stdout.write("Exiting ...\n")
		exit()
	
	# check if outputdir exists
	if os.path.isdir(args["outputdir"]): # if outputdir exists
		
		# check for write access first
		if not os.access(args["outputdir"], os.W_OK):
			sys.stdout.write("No write access to output directory, please choose another directory and try again.\n")
			sys.stdout.write("Exiting ...\n")
			exit()
		
		# check if dir empty
		if not os.listdir(args["outputdir"]): # empty but existing
			pass
			
		else: # existing but not empty? try to find project files
			(project_file, backup_file) = check_project_files(args["outputdir"])
			
			if project_file is not None: # loadable project exists, ask if it should be loaded
				sys.stdout.write("WARNING: The output directory contains a loadable project!\n")
				go_on = ""
				while go_on not in ["l", "load", "n", "new"]:
					if args["quiet"]: # if quiet run -> load project
						(project, error) = load_project(project_file, backup_file) # setting project here already, project run should start after the checks
						project_loaded = True # setting flag since we will skip all the upcoming checks because we use the saved data anyway

						# just some feedback for the user
						if error == 0:
							sys.stdout.write("Project loaded successfully.\n")
						elif error == 1:
							sys.stdout.write("Newest project file not loadable, continuing from backup.\n")
						elif error == 3:
							sys.stdout.write("Found 1 project file which could not be loaded, the file might be corrupt.\n")
							sys.stdout.write("Exiting ...\n")
							exit()
						elif error == 4:
							sys.stdout.write("Found 2 project files, none could be loaded, the files might be corrupt.\n")
							sys.stdout.write("Exiting ...\n")
							exit()
						break

					else: # if not quiet run -> ask user
						go_on = raw_input("Do you wish to load and continue the saved project or start a new one (possibly overwriting old data)? (load/new)\n")
						if go_on in ["l", "load"]: # loading a project
							(project, error) = load_project(project_file, backup_file) # setting project here already, project run should start after the checks
							project_loaded = True # setting flag since we will skip all the upcoming checks because we use the saved data anyway

							# just some feedback for the user
							if error == 0:
								sys.stdout.write("Project loaded successfully.\n")
							elif error == 1:
								sys.stdout.write("Newest project file not loadable, continuing from backup.\n")
							elif error == 3:
								sys.stdout.write("Found 1 project file which could not be loaded, the file might be corrupt.\n")
								sys.stdout.write("Exiting ...\n")
								exit()
							elif error == 4:
								sys.stdout.write("Found 2 project files, none could be loaded, the files might be corrupt.\n")
								sys.stdout.write("Exiting ...\n")
								exit()

						elif go_on in ["n", "new"]: # just skipping we check all the other parameters first before actually creating a project
							pass
						
			else: # existing, not empty, but no loadable project
				sys.stdout.write("WARNING: The selected output directory is not empty, if you continue, data might be overwritten and lost.\n")
				go_on = ""
				while not go_on in ["y", "ye", "yes", "n", "no"] and not args["quiet"]:
					go_on = raw_input("Do you wish to continue? (yes/no)\n")
					if go_on in ["y", "ye", "yes"]:
						break
					elif go_on in ["n", "no"]:
						sys.stdout.write("Exiting ...\n")
						exit()
						
	else: # if outputdir doesn't exist, ask if it should be created and used
		sys.stdout.write("WARNING: The selected output directory does not exist.\n")
		go_on = ""
		if not args["quiet"]:
			while not go_on in ["y", "ye", "yes", "n", "no"]:
				go_on = raw_input("Try to create output directory and go on? (yes/no)\n")
				if go_on in ["y", "ye", "yes"]:
					try:
						os.makedirs(args["outputdir"])
					except OSError:
						sys.stdout.write("Could not create output directory, please choose another directory and try again.\n")
						sys.stdout.write("Exiting ...\n")
						exit()
				elif go_on in ["n", "no"]:
					sys.stdout.write("Exiting ...\n")
					exit()
		else: # silent
			try:
				os.makedirs(args["outputdir"])
			except OSError:
				sys.stdout.write("Could not create output directory, please choose another directory and try again.\n")
				sys.stdout.write("Exiting ...\n")
				exit()
				
	# other checks for input parameters 
	if not project_loaded: # don't need to do all the following tests if we use the saved data in the project file anyway
		
		# check inputfile for existence
		if not os.path.isfile(args["inputfile"]):
			sys.stdout.write("No input file specified or specified input file does not exist.\n")
			sys.stdout.write("Exiting ...\n")
			exit()

		# check inputfile for short reads
		sys.stdout.write("Checking input file\n")
		if not input_file_check(args["inputfile"]) and not args["preprocessing"] and not args["quiet"]:
			sys.stdout.write("WARNING: The selected input file contains short reads (< 30 bases) which can cause a LOT of data to be produced later on, due to rather unspecific alignments. Preprocessing the file is HIGHLY recommended. You can use any third-party program or the basic preprocessing this program provides (-P).")
			go_on = ""
			while not go_on in ["y", "ye", "yes", "n", "no"]:
				go_on = raw_input("Do you wish to go on with the current, unaltered input file (not recommended!)? (yes/no)\n")
				if go_on in ["y", "ye", "yes"]:
					pass
				elif go_on in ["n", "no"]:
					sys.stdout.write("Exiting ...\n")
					exit()

		# check databases
		args["database"] = whole_database_check(args["database"])
		if not args["database"]:
			sys.stdout.write("No valid databases given to perform a program run.\n")
			sys.stdout.write("Exiting ...\n")
			exit()
		
		# check background databases, these are optional, so don't really care if empty
		args["background"] = whole_database_check(args["background"])
		
		# check that at least one method is given
		if not args["method"]:
			sys.stdout.write("No annotation method given.\n")
			sys.stdout.write("Exiting ...\n")
			exit()			

		# check arguments for annotation methods
		for index, each in enumerate(args["arguments"]):
			if (not ":fg:" in each) and (not ":bg:" in each):
				sys.stdout.write(each + " is not a valid argument of the format <method name>:<bg/fg>:<parameters> and will be ignored.\n")
				args["arguments"][index] = "---"
		while "---" in args["arguments"]: # oh boy, the quality of the code these days
			args["arguments"].remove("---")

		# create project
		project = Project.Project(args)
		# create temp folder
		if not os.path.isdir(args["tempdir"]):
			os.makedirs(args["tempdir"])

	# finally run the project
	if not args["reevaluate"]:
		project.run()
	else:
		if not project.last_reads_file:
			sys.stdout.write("The project you try to re-evaluate doesn't seem to be completed. Please finish the project first!\n")
			sys.stdout.write("Exiting ...\n")
			exit()
		project.reevaluate(args)

def get_parameters_from_config_file(configfile):
	# checks if the configfile given is a valid file and then tries to put the parameters in a list that is parseable py argparse
	if not os.path.isfile(configfile):
		return []

	cfile = open(configfile)
	res = []
	for line in cfile:
		if line[0] in ["#", "", "\n"]:
			continue
		res.extend(shlex.split(line.rstrip()))

	return res

def combine_input(args, args2):
	# combines input from two argument namespace objects with the FIRST given taking PRECEDENCE over the second in case one argument is given in both. 
	# background and database entries are merged without doubling single entries in those
	# return value is a dictionary instead of a namespace object
	
	help_dict = {}
	
	# go through one dictionary to work through the key names dynamically, which one doesn't matter since they always have the same keys
	for item in args.__dict__: # item is key name / args.__dict__[item] is the value
		
		# for list type values database and background use both inputs, combine them
		if type(args.__dict__[item]) is list:
			help_list = args.__dict__[item]
			help_list.extend(args2.__dict__[item])
			help_list = Utility.unique_elements_list(help_list) # don't need to do any database more than once
			help_dict[item] = help_list
			continue
		
		# for all generic booleans
		if (not args.__dict__[item]) and args2.__dict__[item]:
			help_dict[item] = args2.__dict__[item]
			continue
		else:
			help_dict[item] = args.__dict__[item]
			#continue
	
		# for all generic values that have None as default (inputfile, outputdir, chunksize, configfile), use the input where provided, if both are provided use the higher ranking one (args)
		if (args.__dict__[item] is None) and (args2.__dict__[item] is not None):
			help_dict[item] = args2.__dict__[item]
			continue
		else:
			help_dict[item] = args.__dict__[item]
			continue
		
	return help_dict
	
def explode_database_folders_to_files(lst): # this is probably better done in a single analysis, so we can group related files
	# takes a list of paths and substitutes all the directories in it with the files in those directories
	for index, each in enumerate(lst):
		if os.path.isdir(each):
			lst.pop(index)
			for item in os.listdir(each):
				if os.path.isfile(item):
					lst.insert(index, item)
	return lst

def make_paths_absolute(lst):
	# takes a list of paths to files/directories and returns the list with absolute paths:
	for index, each in enumerate(lst):
		lst[index] = os.path.abspath(each)
	return lst
	
def check_list_items_for_existance(lst):
	#takes a list of paths to files/directories and checks if they exist, for every not existing item, give out a warning; return value is the list with only the existing items kept
	output = []
	for each in lst:
		if os.path.exists(each):
			output.append(each)
		else:
			sys.stdout.write("WARNING: " + each + " does not exist and will be omitted.\n")
	return output

def whole_database_check(db_list):
	# expects a list with paths to files and directories, returns that list in cleaned up form
	# first check if all items are existing files or directories and delete the ones that are not (giving warnings for each deleted item)
	db_list = check_list_items_for_existance(db_list)
		
	# then explode all the directories in the database to the files inside that directory
	db_list = explode_database_folders_to_files(db_list)

	# then make the paths absolute
	db_list = make_paths_absolute(db_list)
	
	# finally remove doubles if any (which might occur because of giving directories and files or relative paths and absolute paths etc.)
	db_list = Utility.unique_elements_list(db_list)

	return db_list

def check_project_files(outputdir): # checks the output directory for loadable project files and gives back a tuple containing the order in which the files should be tried
	file_list = os.listdir(outputdir)
	prj1_exists = False
	prj2_exists = False
	if "psp1.prj" in file_list:
		prj1_exists = True
		prj1 = outputdir+"/psp1.prj"
	if "psp2.prj" in file_list:
		prj2_exists = True
		prj2 = outputdir+"/psp2.prj"

	if not prj1_exists and not prj2_exists:
		return (None, None)
	elif prj1_exists and not prj2_exists:
		return (prj1, None)
	elif not prj1_exists and prj2_exists: # this shouldn't really happen unless the user deleted a project file but a check doesn't hurt
		return (prj2, None)
	elif prj1_exists and prj2_exists: # both exist, try to load newer one first
		if os.path.getmtime(prj1) > os.path.getmtime(prj2):
			return (prj1, prj2)
		else:
			return (prj2, prj1)

def load_project(project_file, backup_file): # try to load a project from file, return the loaded project and an error code if none of the files specified are loadable
	# Return value is tuple of (project object, error code)
	# Error codes: 0 - Executed just fine ; 1 - Executed OK, Backup file loaded ; 2 - No files to load ; 3 - One file found, not loadable ; 4 - Two files found, none loadable
	if project_file is None and backup_file is None: # no files to load
		return (None, 2) # No files to load
	elif not project_file is None and backup_file is None: # only one file to try
		try:
			pfile = open(project_file)
			return (pickle.load(pfile), 0)
		except:
			return (None, 3) # one project file to load but not loadable
	else: # try both files
		try:
			pfile = open(project_file)
			return (pickle.load(pfile), 0)
		except:
			try:
				bfile = open(backup_file)
				return (pickle.load(bfile), 1)
			except:
				return (None, 4) # two files to load but none loadable

def input_file_check(inputfile):
	# checks input file for reads smaller than 36 bases
	# return False if file contains a read smaller than 36 thus "failing" the check
	# returns True if file doesn't contain such short reads
	gen = Utility.read_generator_fastq_file(inputfile)
	block = next(gen, False)

	while block:
		if len(block[1]) < 36:
			return False
		block = next(gen, False)
	return True

def process_ambiguity_settings(ambiguity_list):
	# check ambiguity levels for taxonomic levels and propagate user set values upwards, the value for the lowest user set level is used downwards
	ranks_lookup = ["all", "forma", "varietas", "subspecies", "species", "species_subgroup", "species_group", "subgenus",
									"genus", "infratribe", "subtribe", "tribe", "infrafamily", "subfamily", "family", "superfamily",
									"parvorder", "infraorder", "suborder", "order", "superorder", "infraclass", "subclass", "class",
									"superclass", "subphylum", "phylum", "superphylum", "subkingdom", "kingdom", "superkingdom"]

	temp_dict = {}
	for index, each in enumerate(ambiguity_list):
		temp_list = ambiguity_list[index].split(":")
		tlen = len(temp_list)
		if tlen != 2 and tlen != 3:
			sys.stdout.write(str(ambiguity_list[index])+" is not a valid argument of the format <taxonomic level>:<int> or <taxonomy>:<taxonomic level>:<int> and will be ignored.\n")
		elif temp_list[-2] not in ranks_lookup:
			sys.stdout.write(temp_list[0]+" is not a valid taxonomic level and will be ignored.\n")
		else:
			if tlen == 2:
				if "all" not in temp_dict:
					temp_dict["all"] = {}
				temp_dict["all"][temp_list[-2]] = float(temp_list[-1])/100.0
			else:  # tlen == 3
				if temp_list[-3] not in temp_dict:
					temp_dict[temp_list[-3]] = {}
				temp_dict[temp_list[-3]][temp_list[-2]] = float(temp_list[-1])/100.0

	out_dict = {}
	# propagate values upwards
	for subdict in temp_dict:
		if not subdict in out_dict:
			out_dict[subdict] = {}
		for rank in ranks_lookup:
			if rank in temp_dict[subdict]:
				for i in range(ranks_lookup.index(rank)+1, len(ranks_lookup)): # index+1 because the user sets the difference they want to see on that level but the program compares the level below
					out_dict[subdict][ranks_lookup[i]] = temp_dict[subdict][rank]

	# propagate values downwards / finds lowest user defined taxonomy
	for subdict in temp_dict:
		lowest = (len(ranks_lookup), None)
		for key in temp_dict[subdict]:
			key_index = ranks_lookup.index(key)
			if key_index < lowest[0]:
				lowest = (key_index, key)

		# propagate values downwards / set lower levels
		for i in range(0, lowest[0]+1): # index+1 for above reasons
			out_dict[subdict][ranks_lookup[i]] = temp_dict[subdict][lowest[1]]

	return out_dict

if __name__ == "__main__":
	main()