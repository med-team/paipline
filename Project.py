import os
import pickle
import shutil
import sys
import methods
import Preprocessing
import Stats
import Utility
import time

class Project:
	# main class for the project, contains list of analyses to do and completion status of those analyses
	def __init__(self, args):
		# args is a dictionary containing the parameters from the config/commandline, including the output directory, input fastq file and so on
		self.args = args
		
		# help variables
		self.analyses = [] # analyses list to be filled later on
		self.analyses_done = []
		self.background = [] # analyses list for the background to be filled later on
		self.background_done = []
		self.methods = []
		self.inputfile_spiked = False
		self.inputfile_preprocessed = False
		self.inputfile_sorted = False
		self.analysis_list_built = False
		self.annotated_merged = False
		self.blast_validation_done = False
		self.unannotated_merged = False
		self.cleaned_up_fg = False
		self.background_analyses_built = False
		self.background_merged = False
		self.cleaned_up_bg = False
		self.background_removed = False
		self.final_to_fastq = False
		self.stats_produced = False
		self.iteration_initiation = False
		self.iterative_stats_produced = False
		self.blast_verification_analyses = []
		self.merge_results = []
		self.blast_files_merged = False
		self.checked_against_each_other = False
		self.last_stats_file = ""
		self.counter = 0
		self.last_reads_file = ""
		self.fg_dict = {} # dictionaries for arguments for annotation methods
		self.bg_dict = {}
		self.fastas_produced = False
		self.subdir = ""
		self.final_stats_moved = False
		self.sample_stats = ""
		self.times = [[time.time(), "Start"]]

		# check which parts of the program/methods to run
		self.methods = self.create_method_list()

		# changing the arguments for called annotation methods to the format we need
		self.arguments_for_methods()
		
	def run(self):
		# main method for running a project, works through the analyses, checks which parts of a project need to be done, saves between the single analysis methods and so on

		while len(self.methods) > 0:
			
			# get current method's name
			current_method_name = eval("methods."+self.methods[0]+"."+self.methods[0]+".__name__")
			
			# add internal control if user desires:
			if self.args.has_key("internal-control") and not self.inputfile_spiked:
				Preprocessing.spike_fastq_file_with_control(self.args["inputfile"], self.args["internal-control"], self.args["outputdir"]+"/"+Utility.short_file_name_without_extension(self.args["inputfile"])+"_spiked.fastq")
				self.args["inputfile"] = self.args["outputdir"]+"/"+Utility.short_file_name_without_extension(self.args["inputfile"])+"_spiked.fastq"
				self.inputfile_spiked = True
				self.times += [[time.time(), "Spiking"]]
				self.save_project()

			# preprocess file if user desires
			if self.args["preprocessing"] and not self.inputfile_preprocessed:
				sys.stdout.write("Preprocessing input file\n")
				Preprocessing.basic_quality_assurance_check_fastq_file(self.args["inputfile"], self.args["outputdir"]+"/"+Utility.short_file_name_without_extension(self.args["inputfile"])+"_prep.fastq")
				if self.inputfile_spiked:
					os.remove(self.args["inputfile"]) # remove spiked input file, after creating the processed one
				self.args["inputfile"] = self.args["outputdir"]+"/"+Utility.short_file_name_without_extension(self.args["inputfile"])+"_prep.fastq"
				self.inputfile_preprocessed = True
				self.times += [[time.time(), "Preprocessing"]]
				self.save_project()

			# sort input file, this has to be done once only, since all the other methods should give out sorted files anyway
			if not self.inputfile_sorted:
				sys.stdout.write("Sorting input file\n")
				Utility.sort_fastq_file(self.args["inputfile"], self.args["outputdir"]+"/"+Utility.short_file_name_without_extension(self.args["inputfile"])+"_sorted.fastq", tmp_dir=self.args["tempdir"])
				# use the sorted file as input file from now on
				if self.inputfile_preprocessed:
					os.remove(self.args["inputfile"]) # remove preprocessed file, after creating the sorted one
				self.args["inputfile"] = self.args["outputdir"]+"/"+Utility.short_file_name_without_extension(self.args["inputfile"])+"_sorted.fastq"
				self.inputfile_sorted = True
				self.times += [[time.time(), "Sorting"]]
				self.save_project()
			
			# build new analysis lists for the current method and "determine the current method" for use as part of output directory
			if not self.analysis_list_built:
				self.analyses = self.create_analyses(self.args["inputfile"], self.args["outputdir"], self.args["database"], self.methods[0])
				self.analysis_list_built = True
				self.times += [[time.time(), "Building analysis list"]]
				self.save_project()
			
			# run all the analyses for the current method
			while len(self.analyses) > 0:
				# run the first analysis in the list
				self.analyses[0].run(gz=True)
				# once done move analysis to done list
				self.analyses_done.append(self.analyses.pop(0))
				# save the project
				self.times += [[time.time(), "Running analyses"]]
				self.save_project()
			
			# merge all the smp files with reads that mapped
			if not self.annotated_merged:
				sys.stdout.write("Merging all files with annotated reads\n")
				help_list = []
				for each in self.analyses_done:
					help_list.append(each.output[0]) # first entry in the output list of the analysis is the annotated reads file
				Utility.merge_smp_files(help_list, self.args["outputdir"]+"/"+current_method_name+"/"+"annotated_reads_merged.smp.gz", True)
				self.annotated_merged = True
				self.times += [[time.time(), "Merging files with annotated reads"]]
				self.save_project()
			
			# merge all the unannotated smp files
			if not self.unannotated_merged:
				sys.stdout.write("Merging all files with unannotated reads\n")
				help_list = []
				for each in self.analyses_done:
					help_list.append(each.output[1]) # second entry in the output list is the unannotated reads file
				Utility.substraction_merge(help_list, self.args["outputdir"]+"/"+current_method_name+"/"+"unannotated_reads_merged.smp.gz", True)
				self.unannotated_merged = True
				self.times += [[time.time(), "Merging files with unannotated reads"]]
				self.save_project()

			# remove obsolete files
			if not self.cleaned_up_fg:
				for each in os.listdir(self.args["outputdir"]+"/"+current_method_name):
					if "annotated_reads.smp.gz" in each: # includes *_unannotated_reads.smp.gz
						os.remove(self.args["outputdir"]+"/"+current_method_name+"/"+each)
				self.cleaned_up_fg = True
				self.times += [[time.time(), "Removing obsolete files"]]
				self.save_project()
				
			# run background analysis with all reads, so we can remove background from unannotated reads for later use aswell
			# otherwise would only use fg mapped reads to save time
			if not self.background_analyses_built:
				self.background.extend(self.create_analyses(self.args["inputfile"], self.args["outputdir"], self.args["background"], self.methods[0], background=False))
				self.background_analyses_built = True
				self.times += [[time.time(), "Building background analysis list"]]
				self.save_project()
			
			# run all the background analyses for the current method, basically equivalent to the actual pathogen search analyses
			while len(self.background) > 0:
				self.background[0].run(gz=True)
				self.background_done.append(self.background.pop(0))
				self.times += [[time.time(), "Running background analyses"]]
				self.save_project()
			
			# merge the background data
			if not self.background_merged and len(self.background_done) > 0: # if no background databases were provided and hence no analyses done, we don't need to merge non-existing files
				help_list = []
				for each in self.background_done:
					help_list.append(each.output[0])
				Utility.merge_smp_files(help_list, self.args["outputdir"]+"/"+current_method_name+"/"+"background_reads_merged.smp.gz", True)
				self.background_merged = True
				self.times += [[time.time(), "Merging background files"]]
				self.save_project()

			# remove obsolete files
			if not self.cleaned_up_bg:
				for each in os.listdir(self.args["outputdir"]+"/"+current_method_name):
					if "annotated_reads.smp.gz" in each: # includes *_unannotated_reads.smp.gz
						os.remove(self.args["outputdir"]+"/"+current_method_name+"/"+each)
				self.cleaned_up_bg = True
				self.times += [[time.time(), "Removing obsolete files"]]
				self.save_project()

			# remove the background from the annotated/unannotated reads
			if not self.background_removed and len(self.background_done) > 0:
				self.last_reads_file = Utility.remove_sequences_from_smp_file(self.args["outputdir"]+"/"+current_method_name+"/"+"annotated_reads_merged.smp.gz", self.args["outputdir"]+"/"+current_method_name+"/"+"background_reads_merged.smp.gz", self.args["outputdir"]+"/"+current_method_name+"/"+"annotated_reads_without_background.smp.gz", True)
				Utility.remove_sequences_from_smp_file(self.args["outputdir"]+"/"+current_method_name+"/"+"unannotated_reads_merged.smp.gz", self.args["outputdir"]+"/"+current_method_name+"/"+"background_reads_merged.smp.gz", self.args["outputdir"]+"/"+current_method_name+"/"+"unannotated_reads_without_background.smp.gz", True)
				self.background_removed = True
				self.times += [[time.time(), "Removing background reads from SMPs"]]
				self.save_project()

			# convert the resulting file to fastq for the next round of analyses (we do this regardless of the fact if there are more rounds of analyses to come to make the file available for manual postprocessing)
			# send unannotated reads including background to next round? probably not!
			if not self.final_to_fastq:
				if len(self.background_done) > 0:
					Utility.smp_to_fastq(self.args["outputdir"]+"/"+current_method_name+"/"+"unannotated_reads_without_background.smp.gz", self.args["outputdir"]+"/"+current_method_name+"/"+"unannotated_reads_without_background.fastq", True)
				else:
					Utility.smp_to_fastq(self.args["outputdir"]+"/"+current_method_name+"/"+"unannotated_reads_merged.smp.gz", self.args["outputdir"]+"/"+current_method_name+"/"+"unannotated_reads_merged.fastq", True)
				self.final_to_fastq = True
				self.times += [[time.time(), "Converting SMP to FastQ for next round of analyses"]]
				self.save_project()

			# initial stats file production
			splits = Stats.splits_family + Stats.splits_genus + Stats.splits_species
			if not self.stats_produced:
				self.subdir = self.args["outputdir"]+"/"+current_method_name # not exactly a subdir, but we need this here to move the right stats files later on
				if len(self.background_done) > 0:
					# for the user
					stats_files = []
					stats_files += Stats.quantify_annotations_in_smp_file(self.args["outputdir"]+"/"+current_method_name+"/"+"annotated_reads_without_background.smp.gz", self.args["outputdir"]+"/"+current_method_name,splits=splits, ambiguity_threshold=self.args["ambiguity"]["all"]["all"], gz=True)
					# for the iterative program part
					self.last_stats_file = Stats.quantify_annotations_in_smp_file(self.args["outputdir"]+"/"+current_method_name+"/"+"annotated_reads_without_background.smp.gz", self.args["outputdir"]+"/"+current_method_name, group_by="family", report_only_grouped=True, gz=True)[0]
				else:
					# for the user
					stats_files = []
					stats_files += Stats.quantify_annotations_in_smp_file(self.args["outputdir"]+"/"+current_method_name+"/"+"annotated_reads_merged.smp.gz", self.args["outputdir"]+"/"+current_method_name, splits=splits, ambiguity_threshold=self.args["ambiguity"]["all"]["all"], gz=True)
					# for the iterative program part
					self.last_stats_file = Stats.quantify_annotations_in_smp_file(self.args["outputdir"]+"/"+current_method_name+"/"+"annotated_reads_merged.smp.gz", self.args["outputdir"]+"/"+current_method_name, group_by="family", report_only_grouped=True, gz=True)[0]
				self.sample_stats = Stats.stats_for_sample(self.args["outputdir"]+"/"+current_method_name, self.args["outputdir"]+"/"+current_method_name+"/stats_for_sample.csv", True)
				all_reads = 0
				with open(self.args["inputfile"]) as infile:
					for line in infile:
						all_reads += 1
				with open(self.sample_stats, "a") as infile:
					infile.write("Input Reads:;"+str(all_reads/4))
				self.stats_produced = True
				self.times += [[time.time(), "Producing initial stats files"]]
				self.save_project()

			# run the iterative part only if the current method needs it!
			if eval("methods."+self.methods[0]+"."+self.methods[0]+".iterative"):

				if self.args["nt"]:
					# blast verification
					if not self.blast_verification_analyses and not self.blast_validation_done:
						virus_refs = Utility.get_references_from_stats_csv_files([self.subdir+"/"+"stats_viruses_species.csv"])
						apicomplexa_refs = Utility.get_references_from_stats_csv_files([self.subdir+"/"+"stats_apicomplexa_species.csv"])
						amoebozoa_refs = Utility.get_references_from_stats_csv_files([self.subdir+"/"+"stats_amoebozoa_species.csv"])
						bacterial_refs = Utility.get_references_from_stats_csv_files([self.subdir+"/"+"stats_bacteria_species.csv"])
						fungi_refs = Utility.get_references_from_stats_csv_files([self.subdir+"/"+"stats_fungi_species.csv"])
						other_refs = Utility.get_references_from_stats_csv_files([self.subdir+"/"+"stats_other.csv"])
						refs = virus_refs + bacterial_refs + amoebozoa_refs + apicomplexa_refs + fungi_refs + other_refs
						if self.background_done:
							self.last_reads_file = self.args["outputdir"]+"/"+current_method_name+"/"+"annotated_reads_without_background.smp.gz"
						else:
							self.last_reads_file = self.args["outputdir"]+"/"+current_method_name+"/"+"annotated_reads_merged.smp.gz"
						Utility.build_fastq_for_references(self.last_reads_file, refs, self.subdir+"/reads_to_validate.fastq", uniques_only=True, gz=True)
						for nt_ref in [self.args["nt"]+"/"+x for x in sorted(os.listdir(self.args["nt"])) if os.path.isfile(self.args["nt"]+"/"+x)]: #for each part of the nt index
							if "Blast" in self.fg_dict:
								self.blast_verification_analyses += [methods.Blast.Blast(self.subdir+"/reads_to_validate.fastq", nt_ref, self.subdir, self.args["tempdir"], self.args["threads"], parameters=self.fg_dict["Blast"], indexdirs=self.args["preindexed"])]
							else:
								self.blast_verification_analyses += [methods.Blast.Blast(self.subdir+"/reads_to_validate.fastq", nt_ref, self.subdir, self.args["tempdir"], self.args["threads"], indexdirs=self.args["preindexed"])]
						self.merge_results = []
						self.blast_validation_done = True
						self.times += [[time.time(), "Building BLAST validation list"]]
						self.save_project()
					while self.blast_verification_analyses:
						self.merge_results += [self.blast_verification_analyses[0].run(gz=True)[0]]
						self.blast_verification_analyses.pop(0)
						self.times += [[time.time(), "Running BLAST validation"]]
						self.save_project()
					if not self.blast_files_merged:
						redundant_blast_files = [self.subdir+"/Blast/"+x for x in sorted(os.listdir(self.subdir+"/Blast")) if x.endswith("annotated_reads.smp.gz")] # includes unannotated
						blast_of_viral_reads = Utility.merge_smp_files(self.merge_results, self.subdir+"/Blast/merged_annotated_reads.smp.gz", True)
						for redundant in redundant_blast_files:
							os.remove(redundant)

						# merge blast results with previous results
						self.last_reads_file = Utility.merge_smp_files([blast_of_viral_reads, self.last_reads_file], self.subdir+"/Blast/annotated_reads_merged_with_blast_data.smp.gz", True)
						self.blast_files_merged = True
						self.times += [[time.time(), "Merging BLAST results with previous results"]]
						self.save_project()

					self.checked_against_each_other = True
					self.times += [[time.time(), "Cross validating (not really)"]]  # if we ever take cross validation back into the flow this needs to be changed ^^
					self.save_project()

			# reset all the help variables and start the next method, fill up the analyses arrays, clean the analyses_done etc., use unannotated reads merged from the last method as inputfile for the next
			if len(self.methods) > 1: # only reset if we have more rounds to go, otherwise keep our last variables for look up purposes
				self.reset()
			
			# finally set the new input file for the next round of analyses
			self.args["inputfile"] = self.args["outputdir"]+"/"+current_method_name+"/"+"unannotated_reads_merged.fastq"
			
			# remove the current method as it is finished
			self.methods.pop(0)
						
			#can't save often enough
			self.times += [[time.time(), "Finishing method round"]]
			self.save_project()

		# if we are done we move the last generated stats and fastas etc. to outputdir/stats
		if not self.final_stats_moved:
			final_stats_folder = self.args["outputdir"]+"/final_output"
			if not os.path.isdir(final_stats_folder):
				os.makedirs(final_stats_folder)
			list_of_folders_to_move = [x for x in sorted(os.listdir(self.subdir)) if os.path.isdir(self.subdir+"/"+x) if x.startswith("fastas") and os.path.isdir(self.subdir+"/"+x)]
			for folder_to_move in list_of_folders_to_move:
				shutil.move(self.subdir+"/"+folder_to_move, final_stats_folder+"/"+os.path.basename(self.args["outputdir"])+"_"+folder_to_move)

			# move stats_for_sample
			shutil.copy(self.sample_stats, self.args["outputdir"]+"/final_output/"+os.path.basename(self.args["outputdir"])+"_"+os.path.basename(self.sample_stats))

			# creating one final stat file with our new method here
			tree = Stats.quantify_annotations_in_smp_file_by_tree(self.last_reads_file, self.args["outputdir"]+"/final_output/"+os.path.basename(self.args["outputdir"])+"_tree_stats.csv", ambiguity_dict=self.args["ambiguity"], neg_list=self.args["uninteresting_list"], gz=True)
			unambiguous_fastas_10_path = self.args["outputdir"]+"/final_output/"+os.path.basename(self.args["outputdir"])+"_"+"fastas_unambiguous_10"
			unambiguous_fastas_all_path = self.args["outputdir"]+"/final_output/"+os.path.basename(self.args["outputdir"])+"_"+"fastas_unambiguous_all"
			if not os.path.isdir(unambiguous_fastas_10_path):
				os.makedirs(unambiguous_fastas_10_path)
			tree.unambiguous_fastas_from_tree_including_lower_levels(unambiguous_fastas_10_path, amount=10)
			if not os.path.isdir(unambiguous_fastas_all_path):
				os.makedirs(unambiguous_fastas_all_path)
			tree.unambiguous_fastas_from_tree_including_lower_levels(unambiguous_fastas_all_path, amount=0)

			# timings
			self.times += [[time.time(), "Generating final stats"]]
			with open(final_stats_folder+"/timings.csv", "w") as timefile:
				timefile.write(";".join(["Step", "Time taken [s]\n"]))
				for i in range(1, len(self.times)):
					timefile.write(";".join([self.times[i][1], str(float(self.times[i][0])-float(self.times[i-1][0])).replace(".", ",")+"\n"]))

			self.final_stats_moved = True
			self.save_project()

		###
		shutil.rmtree(self.args["tempdir"], ignore_errors=True)
		sys.stdout.write("Project completed.\n")
		###

	def reset(self):
		self.analysis_list_built = False
		self.annotated_merged = False
		self.unannotated_merged = False
		self.background_analyses_built = False
		self.background_merged = False
		self.background_removed = False
		self.final_to_fastq = False
		self.stats_produced = False
		self.iteration_initiation = False
		self.iterative_stats_produced = False
		self.blast_verification_analyses = []
		self.merge_results = []
		self.blast_validation_done = False
		self.blast_files_merged = False
		self.checked_against_each_other = False
		self.analyses = []
		self.analyses_done = []
		self.background = []
		self.background_done = []
		self.counter = 0
		self.last_stats_file = ""
		self.last_reads_file = ""
		self.cleaned_up_fg = False
		self.cleaned_up_bg = False
		self.fastas_produced = False

	def save_project(self):
		#find out which file to save to and save, overwriting the older file if both already exist
		prj1_exists = False
		prj2_exists = False
		project_file = None
		prj1 = self.args["outputdir"]+"/psp1.prj"
		prj2 = self.args["outputdir"]+"/psp2.prj"
		if os.path.isfile(prj1):
			prj1_exists = True
		if os.path.isfile(prj2):
			prj2_exists = True

		if not prj1_exists and not prj2_exists:
			project_file = open(prj1, "w")
		elif prj1_exists and not prj2_exists:
			project_file = open(prj2, "w")
		elif not prj1_exists and prj2_exists:
			project_file = open(prj1, "w")
		elif prj1_exists and prj2_exists: # if both exist, overwrite the older one
			if os.path.getmtime(prj1) > os.path.getmtime(prj2):
				project_file = open(prj2, "w")
			else:
				project_file = open(prj1, "w")

		#now actually save the project using pickle
		pickle.dump(self, project_file)

	def create_analyses(self, inputfile, outputdir, file_list, method, background=False):
		# method needs to be a string refering to an allowed annotation method
		# creates a list of objects out of the given file list and method
		output_list = []

		if self.args["preindexed"]: # see if any folders with preindexed files were given
			preindexed = self.args["preindexed"]
		else: # no preindex dir
			preindexed = []

		# checking the foreground / background mapping parameters depending on what we need here
		if not background:
			if self.fg_dict.has_key(method): # see if any parameters for current method were given
				arguments = self.fg_dict[method]
			else: # no parameters
				arguments = ""
		else:
			if self.bg_dict.has_key(method): # see if any parameters for current method were given
				arguments = self.bg_dict[method]
			else: # no parameters
				arguments = ""

		for each in file_list:
				output_list.append(eval("methods."+method+"."+method)(inputfile, each, outputdir, self.args["tempdir"], self.args["threads"], parameters=arguments, indexdirs=preindexed, bg=background))

		return output_list

	def create_method_list(self):
		# creates the method list for the current project
		# checks dynamically against the methods provided as classes in the subfolder methods of this script

		# create a list of allowed options
		allowed_methods = []
		for each in os.listdir(os.path.dirname(os.path.realpath(__file__))+"/methods"):
			if not (each == "__init__.py" or each == "AnnotationMethod.py") and each.endswith(".py"): # ignore package init, parent class module and all files that are not .py files (f.e. .pyc)
				allowed_methods.append(each[:-3])

		# process the command line input here
		method_list = []
		any_illegal = False

		for each in self.args["method"]:
			# to make this case insensitive, we convert each given element to our style of naming annotation method classes, i.e. Capital starting letter, lowercase rest (Blast, Bowtie2 and so on)
			each_s = each[0].upper()+each[1:].lower()

			# checking if allowed, print out if not
			if not each_s in allowed_methods:
				sys.stdout.write("Omitting "+each_s+", as it is not a valid method.\n")
				any_illegal = True
			else:
				method_list.append(each_s)
		if any_illegal:
			sys.stdout.write("Currently implemented methods are: "+", ".join(allowed_methods)+"\n")

		return method_list

	def arguments_for_methods(self):
		# converts the arguments given as strings for the annotation methods on the commandline to dictionaries:
		for each in self.args["arguments"]:
			split = each.split(":")
			split[0] = split[0][0].upper()+split[0][1:].lower() # case insensitive handling of provided method name
			if split[1] == "fg":
				self.fg_dict[split[0]] = split[2]
			else:
				self.bg_dict[split[0]] = split[2]

	def reevaluate(self, args):
		# overwrite important settings with settings from args
		self.args["ambiguity"] = args["ambiguity"]
		if args["uninteresting_list"]:
			self.args["uninteresting_list"] = args["uninteresting_list"]

		# produce new stats and fastas
		tree = Stats.quantify_annotations_in_smp_file_by_tree(self.last_reads_file, self.args["outputdir"]+"/final_output/"+os.path.basename(self.args["outputdir"])+"_tree_stats.csv", ambiguity_dict=self.args["ambiguity"], neg_list=self.args["uninteresting_list"], gz=True)
		unambiguous_fastas_10_path = self.args["outputdir"]+"/final_output/"+os.path.basename(self.args["outputdir"])+"_"+"fastas_unambiguous_10"
		unambiguous_fastas_all_path = self.args["outputdir"]+"/final_output/"+os.path.basename(self.args["outputdir"])+"_"+"fastas_unambiguous_all"
		if not os.path.isdir(unambiguous_fastas_10_path):
			os.makedirs(unambiguous_fastas_10_path)
		tree.unambiguous_fastas_from_tree_including_lower_levels(unambiguous_fastas_10_path, amount=10)
		if not os.path.isdir(unambiguous_fastas_all_path):
			os.makedirs(unambiguous_fastas_all_path)
		tree.unambiguous_fastas_from_tree_including_lower_levels(unambiguous_fastas_all_path, amount=0)

