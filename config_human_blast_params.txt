# Comment lines starting with a hashtag (#) are ignored, parameters are given by the same names as on the command line
# REMINDER: all paths will be used RELATIVE to the current working directory ON PROGRAM START,
# NOT relative to the location of this file, if you want this, start the program from the location of this file

-d /home/andruscha/data/databases_new/viridae/viridae.fa
-d /home/andruscha/data/databases_new/bacteria/bacteria.fa
-d /home/andruscha/data/databases_new/amoebozoa/amoebozoa.fa
-d /home/andruscha/data/databases_new/apicomplexa/apicomplexa.fa
-b /home/andruscha/data/databases_new/homo_sapiens/homo_sapiens.fa
-b /home/andruscha/data/databases_new/artificial_sequences/artificial_sequences.fa
-p /home/andruscha/data/references_new/bacteria/family
-p /home/andruscha/data/references_new/bacteria/other
-p /home/andruscha/data/references_new/viridae/family
-p /home/andruscha/data/references_new/apicomplexa/family
-p /home/andruscha/data/references_new/amoebozoa/family
-g family
-m Bowtie2
-a "Bowtie2:fg:--very-sensitive --no-hd -p 10"
-a "Bowtie2:bg:--very-sensitive --no-hd -p 10"
-a "Blast:fg:-word_size 11 -evalue 10 -reward 2 -penalty -3 -gapopen 5 -gapextend 2"
-a "Blast:bg:-word_size 11 -evalue 10 -reward 2 -penalty -3 -gapopen 5 -gapextend 2"
-t 10
-G
-P
-n /home/andruscha/data/databases_new/nt/5parts
-u /home/andruscha/apathogens/clean_list.txt
-A family:3
-A genus:2
-A species:1
