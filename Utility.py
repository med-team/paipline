import multiprocessing
import os
import subprocess
import string
import sys
import random
import Stats
import re
import gzip

def sort_fastq_file(inputfile, outputfile, tmp_dir, chunksize=1000000000): #sorts a FASTQ File alphabetically by sequence identifier
	# TODO rework (wiki: external sorting problem), also chunks as big as possible, rework to use base file name in naming of temp files, while we're at it, use the generator please
	# inputfile and outputfile are expected to be STRINGS of paths to files, chunksize in bytes (default~100mb) roughly defines the size of temporary files created
	# also converts space characters in the readname to underscores
	shortname = os.path.splitext(os.path.basename(inputfile))[0]
	filesize = os.path.getsize(inputfile)
	infile = open(inputfile)
	tempfiles = [open(tmp_dir+"/"+shortname+".tmp0", "w")]
	activetempfile = 0
	cluster = []
	chunk = []
	currentchunk = 0
	STATE = "NAME" # NAME, SEQUENCE, SPACER, QUALITIES
	bytesread = 0
	linesread = 0

	# calculate chunksize if not user set
	#if chunksize == 1000000000:
	#	chunksize = calculate_optimal_chunk_size()

	# open input file and process line wise, finite state machine
	sequence = ""
	qualities = ""
	lines = 0
	for line in infile:
		linesread += 1
		bytesread += len(line)
		currentchunk += len(line)
		if linesread % 160 == 0:
			sys.stdout.write("Read " + str(100*bytesread/filesize) + "% of the file\r")
		if STATE == "NAME":
			corrected_name = line.translate(string.maketrans(" ", "_")) # this is where space characters are replaced by underscores obviously
			chunk.append(corrected_name) #just append the sequence name and we're done here
			STATE = "SEQUENCE"
		elif STATE == "SEQUENCE":
			if line[0] != "+": #normal sequence line
				sequence += line # keep adding the current line to the whole sequence
				lines += 1 # keep track of how many sequence lines there are
				sequence = sequence.rstrip() #remove line breaks
			else: # spacer line
				chunk.append(sequence+"\n") # since we have the whole sequence now, add it to the chunk, don't forget the linebreak though
				sequence = ""
				chunk.append("+\n") # append the spacer / chunk.append(line) as alternative
				STATE = "QUALITIES"
		elif STATE == "QUALITIES":
			if lines > 1: # every qualities line except the last one
				qualities += line # append every line to the whole string
				lines -= 1 # countdown lines ... we assume there are as many qualities lines as there are sequence lines
				qualities = qualities.rstrip("\n\r") #remove line breaks
			else: # last quality line
				qualities += line # yes this needs to be added aswell, no need for a line break removal this time though
				lines -= 1                #
				chunk.append(qualities)   #
				qualities = ""            # Reset variables and build cluster which contains all sequence information
				cluster.append(chunk)     #
				chunk = []                #
				STATE = "NAME"
				if currentchunk >= chunksize: # if cluster array bigger than chunksize, sort it, write out to current temp file and move on to next temp file
					cluster.sort()
					for index, item in enumerate(cluster): # go through the outer array [ N * [ sequence identifier, sequence, +, qualities ] ]
						for iitem in cluster[index]: # go through inner array item wise
							tempfiles[activetempfile].write(iitem) # write each item to the temp file
					cluster = []
					currentchunk = 0
					activetempfile += 1
					tempfiles.append(open(tmp_dir+"/"+shortname+".tmp"+str(activetempfile), "w"))
	#write out the last cluster
	cluster.sort()
	for index, item in enumerate(cluster): # go through the outer array [ N * [ sequence identifier, sequence, +, qualities ] ]
		for iitem in cluster[index]: # go through inner array item wise
			tempfiles[activetempfile].write(iitem) # write each item to the temp file
	#ok now we have temp files as big as chunksize with sequences in the fastq format sorted by sequence reference

	# close all files to reopen for reading (yes this is necessary, because modes "r+", "w+" and "a+" behave like their binary variants "rb+" etc. :/
	for index, each in enumerate(tempfiles):
		tempfiles[index].close()
	for index, each in enumerate(tempfiles):
		tempfiles[index] = open(tmp_dir+"/"+shortname+".tmp"+str(index), "r")

	# create an array containing [ N * [ filedescriptor, sequence identifier, sequence, spacer (+), qualities ] ] for comparison
	helplst = []
	comparison = []
	for index, each in enumerate(tempfiles):
		helplst.append(tempfiles[index])
		helplst.append(tempfiles[index].readline()) # read in sequence identifier
		helplst.append(tempfiles[index].readline()) # sequence
		helplst.append(tempfiles[index].readline()) # spacer
		helplst.append(tempfiles[index].readline()) # qualities
		comparison.append(helplst)
		helplst = []

	# compare entries to find the next one to write to the output file and read the next sequence from the file it came from
	helpindex = 0
	output = open(outputfile, "w")
	while len(comparison) > 0: # temp files from which all sequences are read will be omitted from the array
		helpindex = find_index_for_next_sequence_identifier_in_comparison_array(comparison)
		output.write(comparison[helpindex][1]) # write the next sequence and stuff to output file
		output.write(comparison[helpindex][2])
		output.write(comparison[helpindex][3])
		output.write(comparison[helpindex][4])

		check = comparison[helpindex][0].readline()    # read the next sequence information from the tempfile
		if check == "":
			comparison.pop(helpindex) # get rid of files that have no more sequences left to read
		else:
			comparison[helpindex][1] = check
			comparison[helpindex][2] = comparison[helpindex][0].readline()
			comparison[helpindex][3] = comparison[helpindex][0].readline()
			comparison[helpindex][4] = comparison[helpindex][0].readline()

	# closing and deleting temp files, closing output file
	for index, each in enumerate(tempfiles):
		tempfiles[index].close()
		os.remove(os.path.abspath(tempfiles[index].name))
	output.close()

def sort_fasta_file(inputfile, outputfile, tmp_dir, chunksize=1000000000): #sorts a FASTQ File alphabetically by sequence identifier
	# inputfile and outputfile are expected to be STRINGS of pathes to files, chunksize in bytes (default~100mb) roughly defines the size of temporary files created
	# also converts space characters in the readname to underscores
	shortname = os.path.splitext(os.path.basename(inputfile))[0]
	filesize = os.path.getsize(inputfile)
	infile = open(inputfile)
	tempfiles = []
	tempfiles.append(open(tmp_dir+"/"+shortname+".tmp0", "w"))
	activetempfile = 0
	cluster = []
	chunk = []
	currentchunk = 0
	STATE = "NAME" # NAME, SEQUENCE
	bytesread = 0
	linesread = 0

	# calculate chunksize if not user set
	#if chunksize == 1000000000:
	#	chunksize = calculate_optimal_chunk_size()

	# open input file and process line wise, finite state machine
	sequence = ""

	for line in infile:
		linesread += 1
		bytesread += len(line)+1
		currentchunk += len(line)+1
		if linesread % 80 == 0:
			sys.stdout.write("Read " + str(100*bytesread/filesize) + "% of the file\r")
		if STATE == "NAME":
			corrected_name = line.translate(string.maketrans(" ", "_")) # this is where space characters are replaced by underscores obviously
			chunk.append(corrected_name) #just append the sequence name and we're done here
			STATE = "SEQUENCE"
		elif STATE == "SEQUENCE":
			if line[0] == ";": # commentary lines are ignored
				pass
			elif line[0] != ">": # normal sequence line
				sequence += line
				sequence = sequence.rstrip("\n\r")
			else: # next name
				chunk.append(sequence+"\n") # append the now complete sequence
				sequence = ""
				cluster.append(chunk)
				chunk = []

				if currentchunk >= chunksize: # if cluster array bigger than chunksize, sort it, write out to current temp file and move on to next temp file
					cluster.sort()
					for index, item in enumerate(cluster): # go through the outer array [ N * [ sequence identifier, sequence ] ]
						for iitem in cluster[index]: # go through inner array item wise
							tempfiles[activetempfile].write(iitem) # write each item to the temp file
					cluster = []
					currentchunk = 0
					activetempfile += 1
					tempfiles.append(open(tmp_dir+"/"+shortname+".tmp"+str(activetempfile), "w"))

				chunk.append(line) # append the next name, sadly the whole fsm seems kinda pointless here, since the STATE NAME will only be used for the very first line

	#write out the last cluster
	chunk.append(sequence+"\n") # append the now complete sequence
	cluster.append(chunk)
	cluster.sort()
	for index, item in enumerate(cluster): # go through the outer array [ N * [ sequence identifier, sequence ] ]
		for iitem in cluster[index]: # go through inner array item wise
			tempfiles[activetempfile].write(iitem) # write each item to the temp file
	# ok now we have temp files as big as chunksize with sequences in the fasta format sorted by sequence reference

	# close all files to reopen for reading (yes this is necessary, because modes "r+", "w+" and "a+" behave like their binary variants "rb+" etc. :/
	for index, each in enumerate(tempfiles):
		tempfiles[index].close()
	for index, each in enumerate(tempfiles):
		tempfiles[index] = open(tmp_dir+"/"+shortname+".tmp"+str(index), "r")

	# create an array containing [ N * [ filedescriptor, sequence identifier, sequence ] ] for comparison
	helplst = []
	comparison = []
	for index, each in enumerate(tempfiles):
		helplst.append(tempfiles[index])
		helplst.append(tempfiles[index].readline()) # read in sequence identifier
		helplst.append(tempfiles[index].readline()) # sequence
		comparison.append(helplst)
		helplst = []

	# compare entries to find the next one to write to the output file and read the next sequence from the file it came from
	helpindex = 0
	output = open(outputfile, "w")
	while len(comparison) > 0: # temp files from which all sequences are read will be omitted from the array
		helpindex = find_index_for_next_sequence_identifier_in_comparison_array(comparison)
		output.write(comparison[helpindex][1]) # write the next sequence and stuff to output file
		output.write(comparison[helpindex][2])

		check = comparison[helpindex][0].readline()    # read the next sequence information from the tempfile
		if check == "":
			comparison.pop(helpindex) # get rid of files that have no more sequences left to read
		else:
			comparison[helpindex][1] = check
			comparison[helpindex][2] = comparison[helpindex][0].readline()

	# closing and deleting temp files, closing output file
	for index, each in enumerate(tempfiles):
		tempfiles[index].close()
		os.remove(os.path.abspath(tempfiles[index].name))
	output.close()

def sort_sam_smp_file(inputfile, outputfile, tmp_dir, chunksize=1000000000):
	#sorts a SAM or SMP file alphabetically by sequence identifier (this is necessary because bowtie2 doesn't always keep reads in the same order as in the input file)
	#inputfile and outputfile are expected to be STRINGS of pathes to files, chunksize in bytes (default~100mb) roughly defines the size of temporary files created
	shortname = os.path.splitext(os.path.basename(inputfile))[0]
	filesize = os.path.getsize(inputfile)
	infile = open(inputfile)
	tempfiles = []
	tempfiles.append(open(tmp_dir+"/"+shortname+".tmp0", "w"))
	activetempfile = 0
	cluster = []
	chunk = []
	currentchunk = 0
	bytesread = 0
	linesread = 0
	header = []

	# calculate chunksize if not user set
	#if chunksize == 1000000000:
	#	chunksize = calculate_optimal_chunk_size()

	for line in infile:
		linesread += 1
		bytesread += len(line)+1
		currentchunk += len(line)+1
		if linesread % 50 == 0:
			sys.stdout.write("Read " + str(100*bytesread/filesize) + "% of the file\r")
		if line[0] == "@": # collect header lines so we can put them into the sorted file first
			header.append(line)
		else: # all other lines
			#for each in line.split("\t", 1): # we only need the readname seperate for sorting, so lets just keep everything else together
			# chunk.append(each)
			cluster.append(line.split("\t", 1))
			#chunk = []
		if currentchunk >= chunksize: # if cluster array bigger than chunksize, sort it, write out to current temp file and move on to next temp file
			cluster.sort() # sort alphabetically after first element of each element
			for index, item in enumerate(cluster): # go through the array ...
				tempfiles[activetempfile].write("\t".join(cluster[index])) # ... and write the line back to the temp file
			cluster = []
			currentchunk = 0
			activetempfile += 1
			tempfiles.append(open(tmp_dir+"/tmp"+str(activetempfile)+".dat", "w"))
	# last chunk
	cluster.sort() # sort alphabetically after first element of each element
	for index, item in enumerate(cluster): # go through the array ...
		tempfiles[activetempfile].write("\t".join(cluster[index])) # ... and write the line back to the temp file

	# close all files to reopen for reading (yes this is necessary, because modes "r+", "w+" and "a+" behave like their binary variants "rb+" etc. :/
	for index, each in enumerate(tempfiles):
		tempfiles[index].close()
	for index, each in enumerate(tempfiles):
		tempfiles[index] = open(tmp_dir+"/tmp"+str(index)+".dat", "r")

	# create an array containing [ N * [ filedescriptor, readname, other, values, too ] ] for comparison
	helplst = []
	comparison = []
	for index, each in enumerate(tempfiles):
		helplst.append(tempfiles[index])
		for each in tempfiles[index].readline().split("\t", 1):
			helplst.append(each)
		comparison.append(helplst)
		helplst = []

	# write file header before sorted data
	output = open(outputfile, "w")
	output.write("".join(header))

	# compare entries to find the next one to write to the output file and read the next sequence from the file it came from
	while len(comparison) > 0: # temp files from which all sequences are read will be omitted from the array
		helpindex = find_index_for_next_sequence_identifier_in_comparison_array(comparison)
		output.write("\t".join(comparison[helpindex][1:])) # write the next readname

		check = comparison[helpindex][0].readline() # read the next sequence information from the tempfile
		if check == "":
			comparison.pop(helpindex) # get rid of files that have no more sequences left to read
		else:
			filehandler = comparison[helpindex][0]
			comparison[helpindex] = []
			comparison[helpindex].append(filehandler)
			for each in check.split("\t", 1):
				comparison[helpindex].append(each)

	# closing and deleting temp files, closing output file
	for index, each in enumerate(tempfiles):
		tempfiles[index].close()
		os.remove(os.path.abspath(tempfiles[index].name))
	output.close()

def is_sorted_fastq_file(inputfile):
	gen = read_generator_fastq_file(inputfile)
	block = next(gen, False)
	lastname = ""

	while block:
		if lastname > block[0]:
			return False
		lastname = block[0]
		block = next(gen, False)

	return True

def is_sorted_fasta_file(inputfile):
	gen = read_generator_fasta_file(inputfile)
	block = next(gen, False)
	lastname = ""

	while block:
		if lastname > block[0]:
			return False
		lastname = block[0]
		block = next(gen, False)

	return True

def is_sorted_sam_smp_file(inputfile):
	#expects a string path to a sam or smp file and returns a boolean value after checking if the file is alphabetically sorted
	infile = open(inputfile)
	lastread = ""
	read = ""

	# check through the file line wise, if we find a read that is alphabetically higher than the one after it, return False for unsorted
	for line in infile:
		lastread = read
		read = line.split("\t", 1)[0].strip()
		if read < lastread:
			if len(read) != 0:
				return False
	return True

def sam_to_fastq_conversion(inputfile, outputfile):
	#expects string paths to two files, converts a sam file to a fastq file
	infile = open(inputfile)
	outfile = open(outputfile, "w")

	for line in infile:
		if line[0] == "@": # Header lines are ignored
			continue
		else:
			elem = line.split("\t")
			outfile.write("".join(["@", elem[0], "\n", elem[9], "\n+\n", elem[10], "\n"]))

def sam_to_fasta_conversion(inputfile, outputfile):
	#expects string paths to two files, converts a sam file to a fasta file
	infile = open(inputfile)
	outfile = open(outputfile, "w")

	for line in infile:
		if line[0] == "@": # Header lines are ignored
			continue
		else:
			elem = line.split("\t")
			outfile.write("".join([">", elem[0], "\n", elem[1], "\n"]))

def fastq_to_fasta_conversion(inputfile, outputfile):
	outfile = open(outputfile, "w")
	gen = read_generator_fastq_file(inputfile)
	block = next(gen, False)

	while block:
		outfile.write(">"+block[0]+"\n"+block[1]+"\n")
		block = next(gen, False)

def calculate_usable_ram():
	# works under UNIX only because of getting the RAM info like this v
	usable_ram = subprocess.Popen("free -b | head  -n 3 | tail -n 1 | tr -s ' ' | cut -d ' ' -f 4", stdout=subprocess.PIPE, shell=True)
	usable_ram = int(usable_ram.communicate()[0]) # total ram as integer in bytes given by free -b

	return usable_ram

def calculate_optimal_chunk_size():
	usable_ram = calculate_usable_ram()

	return usable_ram/4 # use one forth of the usable ram as file size for chunks

def calculate_optimal_cores_to_use():
	# return the number of cores the program will try to use
	cores = multiprocessing.cpu_count()

	if cores == 1:
		return 1
	else:
		return int(cores - cores/2.0) # leave at least 50% of the cores

def find_index_for_next_sequence_identifier_in_comparison_array(comp):
	# expects a comparison array as used in the sortfastqfile and sortfastafile methods [ N * [ file handler, sequence identifier, sequence, ( +, qualities) ] ]
	helplist = []
	for index, each in enumerate(comp):
		helplist.append(comp[index][1])
	return helplist.index(min(helplist))

"""
def remove_sequences_from_smp_file_old(inputfile, inputfile2, outputfile):
	# takes STRING variables for all the pathes, assumes inputfiles to be SORTED! # for background removal

	file1 = open(inputfile)
	file2 = open(inputfile2)
	out = open(outputfile, "w")
	
	read2 = file2.readline().split("\t", 1)[0] # get the first read from file2
	
	for line in file1:
		read = line.split("\t", 1)[0]
		if read == read2: # this is a read we want to remove
			read2 = file2.readline().split("\t", 1)[0] # get a new read from file2
		elif read > read2: # if read2 does not appear in file1
			while read >= read2 and read2 != "":
				read2 = file2.readline().split("\t", 1)[0] # get new read2 until it's bigger or the same as read again
			if read != read2:
				out.write(line)
				read2 = file2.readline().split("\t", 1)[0]
		else:
			out.write(line)

	return os.path.abspath(outputfile)
"""

def remove_sequences_from_smp_file(inputfile, inputfile2, outputfile, gz=False):
	if not gz:
		fgfile = open(inputfile)
		bgfile = open(inputfile2)
		outfile = open(outputfile, "w")
	else:
		fgfile = gzip.open(inputfile, 'rt')
		bgfile = gzip.open(inputfile2, 'rt')
		outfile = gzip.open(outputfile, 'wt')

	fgread = fgfile.readline().rstrip()
	bgread = bgfile.readline().rstrip()
	fgreadname = fgread.split("\t")[0]
	bgreadname = bgread.split("\t")[0]

	while fgread: # go through fg file
		if bgread: # while we have a bg file
			if fgreadname == bgreadname: # if read appears in bg file, skip it, dont write it out
				fgread = fgfile.readline().rstrip()
				bgread = bgfile.readline().rstrip()
				fgreadname = fgread.split("\t")[0]
				bgreadname = bgread.split("\t")[0]
			elif fgreadname < bgreadname: # if read only appears in fg file, write it out and get next one
				outfile.write(fgread+"\n")
				fgread = fgfile.readline().rstrip()
				fgreadname = fgread.split("\t")[0]
			else: #if fgreadname > bgreadname # if read only appears in fg file, get new bg read until we know how to treat this read
				bgread = bgfile.readline().rstrip()
				bgreadname = bgread.split("\t")[0]
		else: # when we don't have more bg reads
			outfile.write(fgread+"\n")
			fgread = fgfile.readline().rstrip()
			fgreadname = fgread.split("\t")[0]

	fgfile.close()
	bgfile.close()
	outfile.close()

	return os.path.abspath(outputfile)

def merge_smp_files(inputfiles_list, outputfile, gz=False):
	# expects inputfiles_list to be a list of paths to SORTED smp files

	# open a temp file
	if not gz:
		outfile = open(os.path.join(os.path.dirname(outputfile), os.path.splitext(os.path.basename(outputfile))[0]+"_temp"+os.path.splitext(os.path.basename(outputfile))[1]), "w")
	else:
		outfile = gzip.open(os.path.join(os.path.dirname(outputfile), os.path.splitext(os.path.basename(outputfile))[0]+"_temp"+os.path.splitext(os.path.basename(outputfile))[1]), "wt")
	# first create an array containing [ [ file handler, readname, sequence, qualities, rest of the line ] * N ]
	comparison_array = []
	for each in inputfiles_list:
		if not gz:
			file_handler = open(each)
		else:
			file_handler = gzip.open(each, 'rt')
		line = file_handler.readline() 
		if line in ["", "\n"]: # empty files will be ignored, this catches files with an empty line at the start as well, so better not use/produce those!
			pass
		else:
			line = line.split("\t", 3) # creates a list with [ readname, sequence, qualities, rest of the line ]
			comparison_array.append([file_handler, line[0], line[1], line[2], line[3]])
	
	# then go through that array, looking for the alphabetically smallest read name
	while len(comparison_array) > 0: # files that reached EOF will be omitted
		helpindex = find_index_for_next_sequence_identifier_in_comparison_array(comparison_array)
		outfile.write("\t".join([comparison_array[helpindex][1], comparison_array[helpindex][2], comparison_array[helpindex][3], comparison_array[helpindex][4].rstrip()])) # should be the whole line without the new line char at the end
	
		# now assuming this read appears in other files too we need to check those as well and append the infos
		indices = [ helpindex ] # if that read appears more than once we need to work through all instances
		smallest_read = comparison_array[helpindex][1]
		for index, each in enumerate(comparison_array):
			if index == helpindex: # skip this case, we don't want to save that hit twice
				continue
			elif each[1] == smallest_read: # found the same read again, append data
				outfile.write("\t"+comparison_array[index][4].rstrip()) # add only the reference specific stuff, since we already have readname, sequence, qualities; without new line char
				indices.append(index)
		outfile.write("\n") # finally add the new line char
		
		#now read in new lines for all the files that had the smallest read on the current line
		for each in indices:
			line = comparison_array[each][0].readline() # read line from the first element of the processed ones for the current smallest read
			if line == "": # i.e. EOF reached
				comparison_array[each] = [] # mark for deletion after this loop is done
			else:
				line = line.split("\t", 3)
				comparison_array[each] = [comparison_array[each][0], line[0], line[1], line[2], line[3]]

		# and finally remove all the files that reached the EOF from the comparison array
		while [] in comparison_array:
			comparison_array.pop(comparison_array.index([]))

	# go through the output file again and throw out double mappings against the same reference ... (a rewrite of this function might be in order to have this done with only walking over the file once)
	outfile.close()
	if not gz:
		infile = open(outfile.name, "r")
		outfile = open(outputfile, "w")
	else:
		infile = gzip.open(outfile.name, "rt")
		outfile = gzip.open(outputfile, "wt")
	for line in infile:
		line_split = line.rstrip().split()
		refs = line_split[3:]
		hit_dict = {}
		for i in range(0, len(refs), 4):
			lowest_exact_taxonomy = refs[i].split("|")[1]
			if not lowest_exact_taxonomy in hit_dict:
				hit_dict[lowest_exact_taxonomy] = [refs[i], refs[i+1], refs[i+2], refs[i+3]]
			else: # need to compare if this hit is better ... if it's only the same or worse, don't consider it, if better replace the current one
				score_in_dict = Stats.read_scoring_system(len(line_split[1]), hit_dict[lowest_exact_taxonomy][1], hit_dict[lowest_exact_taxonomy][2], hit_dict[lowest_exact_taxonomy][3])
				this_score = Stats.read_scoring_system(len(line_split[1]), refs[i+1], refs[i+2], refs[i+3])
				if this_score > score_in_dict:
					hit_dict[lowest_exact_taxonomy] = [refs[i], refs[i+1], refs[i+2], refs[i+3]]

		# make a sorted list out of the dict
		hit_ref_list = []
		for new_ref_dict_key in hit_dict:
			hit_ref_list += [hit_dict[new_ref_dict_key]]
		hit_ref_list = sorted(hit_ref_list, key=sorting_function) # this sorts by the gi number of each reference

		# write out list
		outfile.write("\t".join([line_split[0], line_split[1], line_split[2]]))
		for hit_ref in hit_ref_list:
			outfile.write("\t")
			outfile.write("\t".join([str(x) for x in hit_ref]))
		outfile.write("\n")

	outfile.close()
	# clean up temporary file
	infile.close()
	os.remove(infile.name)

	return outputfile

def add_background_merge(fg_file, bg_file, outputfile):
	# adds hits to reads from the bg_file that appear in the fg_file as well, reads that only appear in the bg_file are ignored
	# expects fg_file to be a string path to a sorted smp file containing foreground reads
	# expects bg_file to be a string path to a sorted smp file containing background reads
	# expects outputfile to be a string path for the file with the merged informations

	outfile = open(outputfile, "w")
	fgfile = open(fg_file)
	bgfile = open(bg_file)

	# first read lines for both input files
	fg_line = fgfile.readline()
	bg_line = bgfile.readline()

	# then go through the fg file and check for existing entries with the same name in the bg file
	while fg_line != "": # still fg lines to process
		fg_array = fg_line.rstrip().split("\t", 3)
		if bg_line != "": # still bg lines to process
			bg_array = bg_line.rstrip().split("\t", 3)
			if fg_array[0] < bg_array[0]:
				# write out fg data only since we won't be getting bg data anymore because the read in bg_array is already bigger
				# get new fg read
				outfile.write("\t".join(fg_array)+"\n")
				fg_line = fgfile.readline()
			elif fg_array[0] == bg_array[0]:
				# merge and write out both reads
				# get new reads for both, if any left
				outfile.write("\t".join(fg_array+[bg_array[3]])+"\n")
				fg_line = fgfile.readline()
				bg_line = bgfile.readline()
			elif fg_array[0] > bg_array[0]:
				# write nothing
				# get new bg read, if any left
				bg_line = bgfile.readline()
		else: # no bg lines left to process
			# write out fg data
			# get new fg read
			outfile.write("\t".join(fg_array)+"\n")
			fg_line = fgfile.readline()

def substraction_merge(inputfiles_list, outputfile, gz=False):
	# takes a list of string paths to SORTED smp inputfiles and only writes out the lines/sequences/reads that appear in ALL files in the list
	# meant for merging several files of UNANNOTATED reads for the next batch of analyses
	if not gz:
		outfile = open(outputfile, "w")
	else:
		outfile = gzip.open(outputfile, "wt")
	working_array = []
	for each in inputfiles_list: # open all the files for reading
		if not gz:
			working_array.append(open(each))
		else:
			working_array.append(gzip.open(each, 'rt'))
	for index, __ in enumerate(working_array):
		line = working_array[index].readline()
		if line in ["", "\n"]: # how to handle empty files? ignore? in principle an empty unannotated reads file produced would mean all reads mapped, so there are no reads that all unannotated files share, we could just return here ...
			return
		else:
			line_array = line.split("\t", 1)
			working_array[index] = [working_array[index], line_array[0], line_array[1]] # expand the dimension of the array, leaves us with an array like this [ N * [ filehandler, readname, rest of the line ] ]

	go_on = True
	while go_on:
		# determine alphabetically highest readname
		help_array = []
		for index, __ in enumerate(working_array):
			help_array.append(working_array[index][1])
		highest_read = max(help_array)

		# go through the array again and read in read names until all the readnames in the array are the same, then write out the line
		all_the_same = True
		for index, each in enumerate(working_array):
			if each[1] < highest_read:
				line = working_array[index][0].readline()
				if line == "": # if EOF is ever reached in any file we are not going to find any more reads that appear in ALL files, so we break
					go_on = False
					break
				line_array = line.split("\t", 1)
				working_array[index] = [working_array[index][0], line_array[0], line_array[1]]
				all_the_same = False
		if all_the_same:
			outfile.write(line) # taking the last read line, which right now is the one with a read name that appears in all files, since we don't know for certain from which file the line came, this method should only ever be used on files with unannotated hits because otherwise we won't know which hits to references are contained in the line
			for index, __ in enumerate(working_array):
				line = working_array[index][0].readline()
				if line == "": # EOF so we break
					go_on = False
					break
				line_array = line.split("\t", 1)
				working_array[index] = [working_array[index][0], line_array[0], line_array[1]]

	outfile.close()
	return outputfile

def short_file_name_without_extension(inputfile):
	return os.path.splitext(os.path.basename(inputfile))[0]

def sam_to_smp_splitter(inputfile, outputdir, give_unannotated=True):
	# convert sam file to own file format smp [name sequence qualities (reference hitlength hitclippings hitmismatches)xN]
	# expects all passed parameters to be string paths
	shortref = short_file_name_without_extension(inputfile)
	fsize = os.path.getsize(inputfile)
	bytesread = 0
	readlines = 0
	annotated_reads = open(outputdir+"/"+shortref+"_annotated_reads.smp", "w")
	if give_unannotated:
		unannotated_reads = open(outputdir+"/"+shortref+"_unannotated_reads.smp", "w")
	readname = ""
	samfile = open(outputdir+"/"+shortref+".sam")

	for line in samfile: # process line wise
		bytesread += len(line)+1
		readlines += 1
		if readlines % 100 == 0:
			sys.stdout.write("Processed " + str(100*bytesread/fsize) + "% of the file\r")
		if line[0]=="@": # ignore header lines
			continue
		else: # process other lines
			last_id = readname
			split = line.split("\t")
			readname = split[0]
			sequence = split[9]
			qualities = split[10]
			reference = split[2]
			if int(split[1])==4: # write unmapped sequences to their respective file / no hitlength or hitmismatches etc
				if give_unannotated:
					unannotated_reads.write("\t".join([readname, sequence, qualities])+"\n")
			else: #write mapped sequences to their respective file / sequences matching more than once to the same line
				cigarlist = cigar_to_list(split[5]) # analyse cigar string for matches, mismatches and clippings
				(hitlength, hitmismatches, clippings) = evaluate_cigar_list(cigarlist)
				if readname == last_id: # if same readname write NEW info to the same line / if this happens at all is dependent on the bowtie2 output, usually it only gives the "best" alignment
					annotated_reads.write("\t".join(["", reference, str(hitlength), str(hitmismatches), str(clippings)]))
				#elif last_id == "": # if first readname write ALL info to line
				#	annotated_reads.write("\t".join([readname, sequence, qualities, reference, str(hitlength), str(hitmismatches), str(clippings)]))
				elif annotated_reads.tell() == 0: # if no sequence has been written to the output file so far
					annotated_reads.write("\t".join([readname, sequence, qualities, reference, str(hitlength), str(hitmismatches), str(clippings)]))
				else:  # every other case, i.e. sequences matching the first time
					annotated_reads.write("\n"+"\t".join([readname, sequence, qualities, reference, str(hitlength), str(hitmismatches), str(clippings)]))
	annotated_reads.write("\n")
	annotated_reads.close()
	if give_unannotated:
		unannotated_reads.close()
		return (os.path.abspath(annotated_reads.name), os.path.abspath(unannotated_reads.name))
	else:
		return (os.path.abspath(annotated_reads.name), "")

def cigar_to_list(cigar):
	# expects a cigar string as found in SAM files, returns a list/array of [ N * [ char, number corresponding to char ] ]
	# should only get cigar strings for sequences that matched, returns an empty list/array if "*" is encountered
	numbers = ""
	lst = []
	for char in cigar:
		if char == "*":
			return []
		if char in "1234567890":
			numbers += char
		else:
			lst.append([char, int(numbers)])
			numbers = ""
	return lst

def evaluate_cigar_list(clist):
	# expects a list/array of the format provided by cigar_to_list, returns a tuple of (matches, mismatches, clippings)
	matches = 0
	mismatches = 0
	clippings = 0

	for item in clist:
		if item[0] == "M": # matches
			matches += item[1]
		elif item[0] in "DIX": # mismatches insertions/deletions
			mismatches += item[1]
		elif item[0] in "HS": # clippings
			clippings += item[1]

	return (matches, mismatches, clippings)

def smp_to_fastq_with_reconstruct_data(inputfile, outputfile):
	# expects a smp file which will be converted to a fastqfile keeping all the hits contained in the smp file neatly behind the + spacer sign between sequence and qualities
	# both parameters are expected to be string paths
	infile = open(inputfile)
	outfile = open(outputfile, "w")

	for line in infile:
			split = line.split("\t", 3)
			outfile.write("".join(["@", split[0], "\n", split[1], "\n+\t", split[3].rstrip(), "\n", split[2], "\n"])) # split[3] gets stripped because it contains line breaks if the file has no annotations

def smp_to_fastq(inputfile, outputfile, gz=False):
	# expects a smp file which will be converted to a fastqfile omitting all the hits contained in the smp file
	# both parameters are expected to be string paths
	if not gz:
		infile = open(inputfile)
	else:
		infile = gzip.open(inputfile)
	outfile = open(outputfile, "w")

	for line in infile:
			split = line.rstrip().split("\t", 3)
			outfile.write("".join(["@", split[0], "\n", split[1], "\n+\n", split[2], "\n"]))

	infile.close()
	outfile.close()

	return outputfile

def collapse_smp_file(inputfile, outputfile):
	# collapses smp files with several identical reads in consecutive lines
	# inputfile and outputfile are expected to be string paths
	# infile needs to be sorted ofc

	infile = open(inputfile)
	outfile = open(outputfile, "w")
	name = ""

	for line in infile:
		lastname = name
		name = line.split()[0]

		if name == lastname:
			split = line.rstrip().split("\t")
			outfile.write("\t"+"\t".join(split[3:])) # omitting the first 3 entries name, seq and qual, cause we should have them already
		else:
			if outfile.tell() != 0: # don't add a new line if we're still at the beginning of the outfile, otherwise "end" the last line
				outfile.write("\n")
			outfile.write(line.rstrip())

	outfile.write("\n") # making sure the file ends in a new line

def fasta_file_chunk_generator(inputfile, num_bases):
	chunk = []
	bases = 0

	gen = read_generator_fasta_file(inputfile)

	pair = next(gen, False)

	while pair:
		if bases >= num_bases:
			yield chunk
			chunk = []
			bases = 0
		chunk.append(pair)
		bases += len(chunk[-1][-1])
		pair = next(gen, False)

	if chunk: # don't forget about the last (non-empty) chunk
		yield chunk

def fastq_file_chunk_generator(inputfile, num_bases):
	chunk = []
	bases = 0

	gen = read_generator_fastq_file(inputfile)

	pair = next(gen, False)

	while pair:
		if bases >= num_bases:
			yield chunk
			chunk = []
			bases = 0
		chunk.append(pair)
		bases += len(chunk[-1][-1]) # using qualities length here, cause it corresponds with the sequence length anyway and is easier to get as last entry
		pair = next(gen, False)

	if chunk: # don't forget about the last (non-empty) chunk
		yield chunk

def read_generator_fasta_file(inputfile):
	infile = open(inputfile)
	chunk = []
	seq = []

	for line in infile:
		if line[0] == ">":
			if chunk:
				chunk[1] = "".join(seq)
				yield chunk
			chunk = [line.rstrip()[1:], ""] # omitting the leading >
			seq = []
		else:
			seq += [line.rstrip()]

	if chunk: # don't forget about the last line
		chunk[1] = "".join(seq)
		yield chunk

def read_generator_fastq_file(inputfile):
	# goes through a fastq file and yields an array of this form [ name/identifier, sequence, spacer, qualities ]
	infile = open(inputfile)
	STATE = "NAME" # NAME, SEQUENCE, QUALITIES
	chunk = []
	sequence = []
	qualities = []
	lines = 0

	for line in infile:
		if STATE == "NAME":
			chunk = [line.rstrip()[1:]] # omitting the leading @
			STATE = "SEQUENCE"
		elif STATE == "SEQUENCE":
			if line[0] != "+": #normal sequence line
				sequence += [line.rstrip()] # keep adding the current line to the whole sequence
				lines += 1 # keep track of how many sequence lines there are
			else: # spacer line
				chunk.append("".join(sequence)) # since we have the whole sequence now, add it to the chunk
				sequence = []
				chunk.append("+") # append the spacer line, omit everything that might be behind the spacer
				STATE = "QUALITIES"
		elif STATE == "QUALITIES":
			if lines > 1: # every qualities line except the last one
				qualities += [line.rstrip()] # append every line to the whole string
				lines -= 1 # countdown lines ... we assume there are as many qualities lines as there are sequence lines
			else: # last quality line
				qualities += [line.rstrip()] 			# yes this needs to be added as well
				lines -= 1               					#
				chunk.append("".join(qualities))  #
				qualities = []	          				# Reset variables and yield chunk
				yield chunk												#
				chunk = []                				#
				STATE = "NAME"										#

def read_generator_smp_file(inputfile, gz=False):
	# yields blocks from an smp file in the following form [readname, sequence, qualities, [[reference1, m, mm, c], [reference2, m, mm, c]]]
	# so that len(block[3]) is the number of references hit ... quite useful for distinguishing between uniques and others ...
	if not gz:
		infile = open(inputfile)
	else:
		infile = gzip.open(inputfile, 'rt')

	for line in infile:
		chunk = []
		line_split = line.rstrip().split("\t")
		chunk += [line_split[0]] # read name
		chunk += [line_split[1]] # sequence
		chunk += [line_split[2]] # qualities
		hits = []
		for index in range(3,len(line_split),4):
			hit = [line_split[index], int(line_split[index+1]), int(line_split[index+2]), int(line_split[index+3])] # hit reference, matches, clippings, mismatches
			hits += [hit]
		chunk += [hits]
		yield chunk

def concatenate_files(inputfile_list, outputfile, gz=False):
	if not gz:
		outfile = open(outputfile, 'w')
	else:
		outfile = gzip.open(outputfile, 'wt')

	for each in inputfile_list:
		if not gz:
			open_file = open(each)
		else:
			open_file = gzip.open(each, 'rt')

		for line in open_file:
			outfile.write(line)

		open_file.close()

	outfile.close()


def split_fasta_file(inputfile, chunk_size, tmp_dir):
	# chunk_size corresponds to the number of bases to use for a chunk, so actual files can be a lot bigger
	shortname = short_file_name_without_extension(inputfile)
	helpint = 0
	tempfiles = []
	tempfilenames = []

	gen = fasta_file_chunk_generator(inputfile, chunk_size)
	chunk = next(gen, False)

	while chunk:
		tempfiles.append(open(tmp_dir+"/"+shortname+"_tmp"+str(helpint)+".fasta", "w"))
		for each in chunk:  # write chunk to the last file in the array
			for index, item in enumerate(each):
				if index == 0: # add an > back to the name
					tempfiles[-1].write(">"+item+"\n")
				else:
					tempfiles[-1].write(item+"\n")
		chunk = next(gen, False)
		helpint += 1

	for each in tempfiles:
		tempfilenames.append(os.path.abspath(each.name))
		each.close()

	return tempfilenames

def split_fastq_file(inputfile, chunk_size, tmp_dir):
	# chunk_size corresponds to the number of bases to use for a chunk, so actual files can be a lot bigger
	shortname = short_file_name_without_extension(inputfile)
	helpint = 0
	tempfiles = []
	tempfilenames = []

	gen = fastq_file_chunk_generator(inputfile, chunk_size)
	chunk = next(gen, False)

	while chunk:
		tempfiles.append(open(tmp_dir+"/"+shortname+"_tmp"+str(helpint)+".fastq", "w"))
		for each in chunk:  # write chunk to the last file in the array
			for index, item in enumerate(each):
				if index == 0: # add an > back to the name
					tempfiles[-1].write("@"+item+"\n")
				else:
					tempfiles[-1].write(item+"\n")
		chunk = next(gen, False)
		helpint += 1

	for each in tempfiles:
		tempfilenames.append(os.path.abspath(each.name))
		each.close()

	return tempfilenames

def dna_sequence_to_protein_sequence(seq):
	# takes a string representing a dna sequence and returns the translated protein sequence as a string

	genecode = {'AAA':'K', 'AAC':'N', 'AAG':'K', 'AAT':'N', 'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T', 'AGA':'R',
							'AGC':'S', 'AGG':'R', 'AGT':'S', 'ATA':'I', 'ATC':'I', 'ATG':'M', 'ATT':'I', 'CAA':'Q', 'CAC':'H',
							'CAG':'Q', 'CAT':'H', 'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P', 'CGA':'R', 'CGC':'R', 'CGG':'R',
							'CGT':'R', 'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L', 'GAA':'E', 'GAC':'D', 'GAG':'E', 'GAT':'D',
							'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A', 'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G', 'GTA':'V',
							'GTC':'V', 'GTG':'V', 'GTT':'V', 'TAA':'*', 'TAC':'Y', 'TAG':'*', 'TAT':'Y', 'TCA':'S', 'TCC':'S',
							'TCG':'S', 'TCT':'S', 'TGA':'*', 'TGC':'C', 'TGG':'W', 'TGT':'C', 'TTA':'L', 'TTC':'F', 'TTG':'L',
							'TTT':'F'}

	proteinseq = ""
	length = len(seq)
	for i in range(0, length, 3):
		if genecode.has_key(seq[i:i+3]): # make sure the key exists, i.e. skip trailing bases that don't form a triplet
			proteinseq += genecode[seq[i:i+3]]

	return proteinseq

def dna_reverse_complement(seq):
	# returns a string with the reverse complement of a given dna sequence string / uses lists inbetween

	lst = list(seq)[::-1] # input string to list and reversed

	for index, base in enumerate(lst):
		if base == "A":
			lst[index] = "T"
		elif base == "C":
			lst[index] = "G"
		elif base == "G":
			lst[index] = "C"
		elif base == "T":
			lst[index] = "A"
		elif base == "R": # puRine (A, G)
			lst[index] = "Y" # pYrimidine (C, T)
		elif base == "Y":
			lst[index] = "R"
		else: # other symbols like N or -
			#raise ValueError
			continue

	return "".join(lst)

def dna_complement(seq):
	# returns a string with the complement of a given dna sequence string / uses lists inbetween

	lst = list(seq) # input string to list and _without_ reversing

	for index, base in enumerate(lst):
		if base == "A":
			lst[index] = "T"
		elif base == "C":
			lst[index] = "G"
		elif base == "G":
			lst[index] = "C"
		elif base == "T":
			lst[index] = "A"
		elif base == "R": # puRine (A, G)
			lst[index] = "Y" # pYrimidine (C, T)
		elif base == "Y":
			lst[index] = "R"
		else: # other symbols like N or -
			#raise ValueError
			continue

	return "".join(lst)


def sample_percentage_of_reads_from_fasta_or_fastq(inputfile, outputfile, percentage=10, static=False):
	# iterates over an input fastq or fasta file and produces an output in which approximately the given percentage of reads are kept
	# percentage can be int or float
	# static flag can be used to always use the same seed for the pseudo-random number generator, so this method might produce the same output when used again with the same parameters
	# returns the number of reads sampled as well as the original number of reads
	p = float(percentage)/100
	reads = 0
	sampled = 0
	outfile = open(outputfile, "w")

	# determine input
	intype = fasta_or_fastq(inputfile)

	# create corresponding iterator and set sequence name line start character
	if intype == 1:
		gen = read_generator_fasta_file(inputfile)
		char = ">"
	else:
		gen = read_generator_fastq_file(inputfile)
		char = "@"

	# get the first block from the input file
	block = next(gen, False)

	# initialise pseudo-random number generator using the name of the first read as seed if static run is wished
	if static:
		randomgen = random.Random(block[0])
	else:
		randomgen = random.Random() # no seed if dynamic

	# now go through the input file and write out
	while block:
		reads += 1
		if randomgen.random() <= p:
			sampled += 1
			for index, each in enumerate(block):
				if index == 0:
					outfile.write(char+each+"\n")
				else:
					outfile.write(each+"\n")
		block = next(gen, False)

	return (sampled, reads)

def sample_number_of_reads_from_fasta_or_fastq(inputfile, outputfile, number=10, static=False):
	# iterates over an input fastq or fasta file and produces an output in which the given number of reads are kept

	reads = 0
	outfile = open(outputfile, "w")

	# determine input
	intype = fasta_or_fastq(inputfile)

	# create corresponding iterator and set sequence name line start character
	if intype == 1:
		gen = read_generator_fasta_file(inputfile)
		char = ">"
	else:
		gen = read_generator_fastq_file(inputfile)
		char = "@"

	# create list with all reads (memory :( )
	blocks = []
	block = next(gen, False)
	while block:
		reads += 1
		blocks += [block]
		block = next(gen, False)

	# initialise pseudo-random number generator using the name of the first read as seed if static run is wished
	if static:
		randomgen = random.Random(blocks[0][0])
	else:
		randomgen = random.Random() # no seed if dynamic

	# get wanted number of reads from the previously created list
	selected = []
	for i in range(number):
		try: # could be less reads overall than desired number, therefore try
			selected += [blocks.pop(randomgen.randint(0, len(blocks)-1))]
		except:
			break

	# write out selected reads
	for read in selected:
		for index, each in enumerate(read):
			if index == 0:
				outfile.write(char+each+"\n")
			else:
				outfile.write(each+"\n")

	return reads # return number of original reads

def fasta_or_fastq(inputfile):
	# checks if a given file is fasta format or fastq format, raises TypeError
	with open(inputfile) as infile:
		first_byte = infile.read(1)
		if first_byte == ">":
			return 1 # fasta file is return code 1
		elif first_byte == "@":
			return 2 # fastq file is return code 2
		else:
			raise TypeError

def unique_elements_list(lst):
	# expects a list and returns a list with only unique elements
	help_list = []
	for item in lst:
		if item not in help_list:
			help_list.append(item)
	return help_list

def generate_fastq_sample_reads_from_fasta_reference(fasta, fastq, number_of_reads=12000000, read_length=150, forward_and_reverse=True, paired_end=False, static=True, paired_end_gaps = [200, 5000]):
	"""
	A method that generates simulated fastq reads from a given fasta reference.

	Keyword arguments:
	fasta -- string path to fasta reference/input file
	fastq -- string path to fastq output file with generated reads
	number_of_reads -- integer giving the desired number of reads to genereate
	read_length -- integer giving the desired read length
	forward_and_reverse -- boolean determining if forward and reverse reads are to be produced; if false, only forward reads are generated
	paired_end -- boolean stating if paired end reads are wished
	static -- boolean determining whether the randomly chosen positions of reads and the gaps between read pairs should be generated using a static/reproducible random generator
	paired_end_gaps -- list with 2 entries defining the range of gaps between 2 paired end reads, according to illumina the gaps are 200 bp to 5 kb big
	"""

	infile = open(fasta)
	outfile = open(fastq, "w")

	# first we need to go through all the fasta entries and determinate how many reads we will generate for each one, based on their relative lengths
	random_gen = False
	list_of_lengths = []
	for line in infile:
		if line[0] == ">": # skip descriptor lines
			if not random_gen and static:	# initiale the random generator using the first descriptor in the fasta file
				random_gen = random.Random(line.rstrip()[1:])
			elif not random_gen:
				random_gen = random.Random()
		else:
			list_of_lengths += [len(line.rstrip())] # add length of sequence to list

	sum_of_lengths = sum(list_of_lengths)
	reads_per_entry = [int(round(float(x)/sum_of_lengths*number_of_reads)) for x in list_of_lengths] # generates a list with integers stating how many reads should be generated for each entry in the fasta file / read number will only be approximately what user defined

	# generate the reads
	counter = 0
	pair_counter = 0
	reference = ""
	infile.seek(0)
	qualities = "".join(["H"]*read_length) # H is a pretty good quality score ^^
	for line in infile:
		if line[0] == ">":
			reference = line.rstrip()[1:]
		else:
			seq = line.rstrip()
			if not paired_end:
				rev_comp = dna_complement(seq[::-1]) # complement of the reversed sequence / reverse complement
				for i in range(reads_per_entry[0]):
					try:
						start_index = random_gen.randint(0, list_of_lengths[0]-1-read_length)
					except ValueError:
						start_index = 0
					counter += 1
					reverse = random_gen.choice([True, False])
					if forward_and_reverse and reverse: # reverse read (if any wanted)
						read_name = "@" + reference + "_rev_" + str(counter)
						read_seq = rev_comp[start_index:start_index+read_length]
					else: # forward read otherwise
						read_name = "@" + reference + "_fwd_" + str(counter)
						read_seq = seq[start_index:start_index+read_length]
					outfile.write("\n".join([read_name, read_seq, "+", qualities[0:len(read_seq)], ""]))
			else: # paired end, ignores forward_and_reverse parameter, cause paired end reads will ALWAYS be forward and reverse pairs
				comp = dna_complement(seq) # complement sequence, not yet reversed
				for i in range(int(round(reads_per_entry[0]/2.0))):
					pair_counter += 1
					gap = random_gen.randint(paired_end_gaps[0], paired_end_gaps[1])
					try:
						start_index_fwd = random_gen.randint(0, list_of_lengths[0]-1-2*read_length-gap) # this only works with sufficiently long reads
					except ValueError:
						start_index_fwd = 0
					start_index_rev = start_index_fwd + gap
					counter += 1
					fwd_name = "@" + reference + "_pair_" + str(pair_counter) + "_fwd_" +str(counter)
					counter += 1
					rev_name = "@" + reference + "_pair_" + str(pair_counter) + "_rev_" +str(counter)
					fwd_seq = seq[start_index_fwd:start_index_fwd+read_length]
					comp_seq = comp[start_index_rev:start_index_rev+read_length]
					rev_comp_seq = comp_seq[::-1]
					outfile.write("\n".join([fwd_name, fwd_seq, "+", qualities[0:len(fwd_seq)], rev_name, rev_comp_seq, "+", qualities[0:len(rev_comp_seq)], ""]))
			reads_per_entry.pop(0)
			list_of_lengths.pop(0)

def get_reads_for_references(smp_file, references, output_folder, number_of_reads=0, uniques_only=True):
	"""
	This function generates fasta files with sequences from reads that hit specific references.

	:param smp_file: string path to input smp file
	:param references: list of reference strings for which fasta files should be created
	:param output_folder: string path to output folder for fasta files
	:param number_of_reads: number of reads for each reference, default = 0, which means report all reads
	:param uniques_only: only give reads that uniquely hit
	:return:
	"""

	#creating a counter for each reference
	counters = len(references)*[0]

	# creating the necessary, empty files
	for each in references:
		open(output_folder+"/"+each+".fasta", "w")

	# populate files / # fixme slow as hell ...
	with open(smp_file) as infile:
		for line in infile:
			line_split = line.rstrip().split("\t")
			line_split2 = line.rstrip().replace("|", "\t").replace(":", "\t").split("\t")
			for index, each in enumerate(references):
				check = False
				for item in line_split2:
					if each in item:
						check = True
						break
				if uniques_only:
					if check and (counters[index] < number_of_reads or number_of_reads == 0) and len(line_split) == 7: # len(line_split) == 7 equals lines with unique hits
						with open(output_folder+"/"+each+".fasta", "a") as outfile:
							outfile.write(">"+line_split[0]+"\n"+line_split[1]+"\n")
						counters[index] += 1
				else:
					if check and (counters[index] < number_of_reads or number_of_reads == 0):
						with open(output_folder+"/"+each+".fasta", "a") as outfile:
							outfile.write(">"+line_split[0]+"\n"+line_split[1]+"\n")
						counters[index] += 1

	return output_folder

def get_reads_for_references_on_level(smp_file, level, output_folder, number_of_reads=0, uniques_only=True):
	"""
	This function generates fasta files with sequences from reads that hit specific references.

	:param smp_file: string path to input smp file
	:param level: taxonomical level on which output is desired
	:param output_folder: string path to output folder for fasta files
	:param number_of_reads: number of reads for each reference, default = 0, which means report all reads
	:param uniques_only: only give reads that hit uniquely
	:return:
	"""

	# initialize
	counters = {}

	# populate files
	with open(smp_file) as infile:
		for line in infile:
			line_split = line.rstrip().split("\t")
			line_split2 = line.rstrip().replace("|", "\t").replace(":", "\t").split("\t")
			indices = [i for i, x in enumerate(line_split2) if x == level]
			refs = []
			for index in indices:
				refs += [line_split2[index+2]]
			unique = False
			if len(line_split) == 7:
				unique = True
			for ref in refs:
				if ref in counters and counters[ref] != number_of_reads:
					if (uniques_only and unique) or not uniques_only:
						with open(output_folder+"/"+ref+".fasta", "a") as outfile:
							outfile.write(">"+line_split[0]+"\n"+line_split[1]+"\n")
							try:
								counters[ref] += 1
							except KeyError:
								counters[ref] = 1

	return output_folder

def get_fastq_reads_for_references(smp_file, references, output_folder, number_of_reads=0, uniques_only=True):
	"""
	This function generates fasta files with sequences from reads that hit specific references.

	:param smp_file: string path to input smp file
	:param references: list of reference strings for which fasta files should be created
	:param output_folder: string path to output folder for fasta files
	:param number_of_reads: number of reads for each reference, default = 0, which means report all reads
	:param uniques_only: only give reads that uniquely hit
	:return:
	"""

	#creating a counter for each reference
	counters = len(references)*[0]

	# creating the necessary, empty files
	for each in references:
		open(output_folder+"/"+each+".fastq", "w")

	# populate files
	with open(smp_file) as infile:
		for line in infile:
			line_split = line.rstrip().split("\t")
			line_split2 = line.rstrip().split("\t|:")
			for index, each in enumerate(references):
				check = False
				for item in line_split2:
					if each in item:
						check = True
						break
				if uniques_only:
					if check and (counters[index] < number_of_reads or number_of_reads == 0) and len(line_split) == 7: # len(line_split) == 7 equals lines with unique hits
						with open(output_folder+"/"+each+".fastq", "a") as outfile:
							outfile.write("@"+line_split[0]+"\n"+line_split[1]+"\n+\n"+line_split[2]+"\n")
						counters[index] += 1
				else:
					if check and (counters[index] < number_of_reads or number_of_reads == 0):
						with open(output_folder+"/"+each+".fastq", "a") as outfile:
							outfile.write("@"+line_split[0]+"\n"+line_split[1]+"\n+\n"+line_split[2]+"\n")
						counters[index] += 1

def get_references_from_stats_csv_files(files):
	references = []
	for each in files:
		with open(each) as infile:
			for line in infile:
				if "Taxonomy" in line: # skip first line
					continue
				references += [line.split(";")[0].split(" ")[0]]
	return references

def build_fastq_for_references(smp_file, references, outputfile, uniques_only=True, max_number_of_reads_per_reference=0, gz=False):
	refs = {}
	if not gz:
		infile = open(smp_file)
	else:
		infile = gzip.open(smp_file, 'rt')
	with open(outputfile, "w") as outfile:
		for line in infile:
			line_split = line.rstrip().split("\t")
			for ref in references:
				if uniques_only:
					if ref in line and len(line_split) == 7:
						if max_number_of_reads_per_reference: # if we have a max given
							try:
								refs[ref] += 1  # add one to that ref's counter
							except KeyError:
								refs[ref] = 1 # ... if the key doesn't exist, this is the first read for this ref and we create the key
							if refs[ref] <= max_number_of_reads_per_reference: # only write while max not reached
								outfile.write("@"+line_split[0]+"\n"+line_split[1]+"\n+\n"+line_split[2]+"\n")
								break
						else: # if no max given, always write
							outfile.write("@"+line_split[0]+"\n"+line_split[1]+"\n+\n"+line_split[2]+"\n")
							break
				else:
					if ref in line:
						if max_number_of_reads_per_reference: # if we have a max given
							try:
								refs[ref] += 1 # add one to that ref's counter
							except KeyError:
								refs[ref] = 1 # ... if the key doesn't exist, this is the first read for this ref and we create the key
							if refs[ref] <= max_number_of_reads_per_reference: # only write while max not reached
								outfile.write("@"+line_split[0]+"\n"+line_split[1]+"\n+\n"+line_split[2]+"\n")
								break
						else: # if no max given, always write
							outfile.write("@"+line_split[0]+"\n"+line_split[1]+"\n+\n"+line_split[2]+"\n")
							break
	infile.close()
								
def split_fasta_to_equal_size_files(inputfile, outputdir, size=4000000000, parts=0):
	# if parts are given, size parameter is ignored ...
	if parts != 0:
		size = os.path.getsize(inputfile) / parts

	gen = read_generator_fasta_file(inputfile)
	length = 0
	counter = 1
	basename = os.path.splitext(os.path.basename(inputfile))[0]
	outfile = open(outputdir+"/"+basename+"_1.fa", "w")
	return_value = [outputdir+"/"+basename+"_1.fa"]
	for block in gen:
		block_length = 0
		if length >= size:
			counter += 1
			outfile.close()
			full_path = outputdir+"/"+basename+"_"+str(counter)+".fa"
			outfile = open(full_path, "w")
			length = 0
			return_value += [full_path]
		for each in block:
			block_length += len(each)
		block_length += 1 # > gets ommited, so we count it in here
		length += block_length
		for index, each in enumerate(block):
			if index == 0:
				outfile.write(">")
			outfile.write(each+"\n")

	return return_value

def expand_cigar_string(cigar_string):
	"""
	This function expands cigar strings like "4S12M1D4M" to "SSSSMMMMMMMMMMMMDMMMM" for easier evaluation of the mapping quality.

	:param cigar_string: A string representing a cigar string as found in sam files
	:return: A string generated from the original cigar string expanded to the sequence length, each letter representing one base of the original sequence
	"""

	number = ""
	result = ""

	for character in cigar_string:
		if character in "0123456789":
			number += character
		else:
			letter = character
			result += int(number)*letter
			number = ""

	return result

def sort_raw_fasta_by_gi_number(inputfile, outputfile, chunksize=100000000, temp_dir="/tmp"):
	"""
	Sorts NCBI BLAST database fastas by their gi number.

	:param inputfile: string path to input fasta file in ncbi format (sequence identifiers starting with >gi|12345678|...)
	:param outputfile: string path to output file
	:param chunksize: size of chunks to be sorted, default 100000000 eq. 100 mb
	:param temp_dir: string path of directory in which files for sorting will be written
	:return: string path to output file again
	"""

	insize = os.path.getsize(inputfile)
	pc_str = ['00', '05', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55', '60', '65', '70', '75', '80', '85', '90', '95']
	pc_floats = [0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95]
	shortname = os.path.basename(os.path.dirname(inputfile))+"_"+os.path.splitext(os.path.basename(inputfile))[0]
	current_temp_file = 0
	temp_files = [open(temp_dir+"/"+shortname+".tmp"+str(current_temp_file).zfill(4), "w")]
	gen = read_generator_fasta_file(inputfile)
	chunk = []
	current_chunksize = 0
	readinsize = 0.0

	for block in gen:
		blocksize = 0
		for each in block:
			blocksize += len(each)

		# reporting block
		readinsize += blocksize+3 # +3 for > and two line breaks ... size might still be off if input fasta file is wrapped
		if pc_floats and readinsize/insize > pc_floats[0]:
			pc_floats.pop(0)
			sys.stdout.write("Read "+pc_str.pop(0)+" % of "+inputfile+"\n")
			sys.stdout.flush()

		if current_chunksize + blocksize > chunksize: # reached chunksize, so we sort by gi and write out
			chunk += [block]
			sys.stdout.write("Sorting chunk and writing to "+temp_files[current_temp_file].name+"\n")
			sys.stdout.flush()
			chunk = sorted(chunk, key=lambda x: int(x[0].split("|")[1])) # sorts by gi number
			for read in chunk:
				temp_files[current_temp_file].write(">"+read[0]+"\n"+read[1]+"\n")
			current_temp_file += 1
			temp_files += [open(temp_dir+"/"+shortname+".tmp"+str(current_temp_file).zfill(4), "w")]
			del chunk
			chunk = []
			current_chunksize = 0
		else: # still space in current chunk, so we add some more blocks
			chunk += [block]
			current_chunksize += blocksize

	# sort and write out last chunk
	sys.stdout.write("Read 100 % of "+inputfile+"\n")
	sys.stdout.write("Sorting chunk and writing to "+temp_files[current_temp_file].name+"\n")
	sys.stdout.flush()
	chunk = sorted(chunk, key=lambda x: int(x[0].split("|")[1])) # sorts by gi number
	for read in chunk:
		temp_files[current_temp_file].write(">"+read[0]+"\n"+read[1]+"\n")

	# reopen all temp files for reading via generator
	gens = []
	for index, temp_file in enumerate(temp_files):
		temp_files[index].close()
		#temp_files[index] = open(temp_files[index].name, "r")
		gen = read_generator_fasta_file(temp_files[index].name)
		gens += [gen]

	# get first entry from each file
	list_of_next_entries = []
	for gen in gens:
		list_of_next_entries += [next(gen, False)]

	print len(list_of_next_entries)
	import time
	time.sleep(3)

	# sorted write to final file
	outfile = open(outputfile, "w")
	sys.stdout.write("Sorting and writing entries from chunks to "+outputfile+"\n")
	sys.stdout.flush()
	while list_of_next_entries:
		print len(list_of_next_entries)
		# find next read to write
		index_of_next_entry = list_of_next_entries.index(min(list_of_next_entries, key=lambda x: int(x[0].split("|")[1])))
		# write it
		outfile.write(">"+list_of_next_entries[index_of_next_entry][0]+"\n"+list_of_next_entries[index_of_next_entry][1]+"\n")
		# get next read from that file
		list_of_next_entries[index_of_next_entry] = next(gens[index_of_next_entry], False)
		# check if all gens still give blocks/reads
		try:
			pop_index = list_of_next_entries.index(False)
			list_of_next_entries.pop(pop_index)
			gens.pop(pop_index)
		except ValueError:
			pass

	# done
	sys.stdout.write("Finished writing, deleting temporary files\n")
	sys.stdout.flush()
	for temp_file in temp_files:
		os.remove(temp_file.name)

	return outputfile

def expand_taxonomy_list(inlist_file, db, outlist_file):
	"""
	Expands a given list with taxonomical entries and adds all subbranches of those as well.

	:param inlist_file: string path to a text file containing a taxonomy list
	:param db: fasta format database used to extract all entries
	:param outlist_file: string path to a text file to be created by this function containing the expanded list
	:return: string path to the newly created expanded list
	"""

	# reading initial file
	in_tax_dict = {}
	with open(inlist_file) as infile:
		for line in infile:
			org = line.rstrip().split()[0]
			try:
				in_tax_dict[org] += 1
			except KeyError:
				in_tax_dict[org] = 1

	# going through db, building a new dict
	gen = read_generator_fasta_file(db)
	out_tax_dict = {}
	for read in gen:
		anno_split = split_annotation_string(read[0])
		for key in in_tax_dict:
			for index, elem in enumerate(anno_split):
				if elem[2] == key:
					for i in range(0, index+1):
						try:
							out_tax_dict[anno_split[i][2]] += 1
						except KeyError:
							out_tax_dict[anno_split[i][2]] = 1
					for i in range(index, -1, -1): # uuh, this is creepy, deleting entries while we technically are still iterating over them / we also delete from back to front so we dont have any shifts and all indices hit the entries they should
						del anno_split[i]
					break # cause we can only find that exact tax name once per string anyway, this is also a must because of the deleting above to avoid any quirks

	# sorting and writing out of new dict
	keys = sorted(out_tax_dict.keys())

	with open(outlist_file, "w") as outfile:
		for key in keys:
			outfile.write(key+"\n")

	for key in in_tax_dict:
		if not key in out_tax_dict:
			print "Nothing found for\t"+key

	return outlist_file

def split_annotation_string(annotation_string):
	"""
	A function that returns lists produced from annotation strings to iterate over.

	:param annotation_string: an annotation string as found in our databases <NCBI annotations>||gi|tax_level:tax_id:name|next_tax_level:next_tax_id:next_name|...
	:return: a list of lists consisting of elements like [tax_level, tax_id, name]
	"""

	annotation_string = re.split("\|\|", annotation_string)[-1]  # our annotation is always the last one
	annotation_string = annotation_string[annotation_string.index("|")+1:] # cutting off the gi number
	split = annotation_string.split("|")
	for index, elem in enumerate(split):
		split[index] = split[index].split(":")
	for index, elem in enumerate(split):
		split[index][1] = int(split[index][1]) # cast taxids to ints

	return split

def fastq_to_fasta_conversion_with_sequence_and_qualities_in_name(inputfile, outputfile):
	outfile = open(outputfile, "w")
	gen = read_generator_fastq_file(inputfile)
	block = next(gen, False)

	while block:
		outfile.write("".join([">",block[0],"|",block[1],"|",block[3],"\n",block[1],"\n"]))
		block = next(gen, False)


def sorting_function(x):
	try:
		return lambda x: int(x[0].split("|")[0])
	except ValueError:
		return 0