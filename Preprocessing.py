import itertools
import sys
import Utility
import subprocess

def spike_fastq_file_with_control(fastqfile, control, outputfile):
    # takes two files and concatenates them, the sequence descriptions of the second file are also tagged as control
    # returns the number of control sequences written to the output file
    control_sequences = 0

    with open(outputfile, "w") as outfile:
        with open(fastqfile) as file1:
            for line in file1:
                outfile.write(line)

        gen = Utility.read_generator_fastq_file(control)
        packet = next(gen, False)

        while packet:
            control_sequences += 1
            packet[0] += " --- PSP INTERNAL CONTROL ---"
            for index, each in enumerate(packet):
                if index == 0:
                    outfile.write("@"+each+"\n")
                else:
                    outfile.write(each+"\n")
            packet = next(gen, False)

    return control_sequences

def get_kmer_freq(seq, k):
    # adapted from wojtek
    kmers = {}
    for pos in range(0,len(seq)-k+1):
        kmer = seq[pos:pos+k]
        if not kmers.has_key(kmer):
            kmers[kmer] = 1
        else:
            kmers[kmer] += 1
    return kmers

def get_dust(seq, k):
    # adapted from wojtek
    kmers = get_kmer_freq(seq, k)
    return (100*sum([(x-1)**2 for x in kmers.values()]))/(len(seq)**2)

def trim_dust(read, k=3, window=20, max_dust=5):
    # read input as [name, sequence, spacer, qualities]
    # adapted from wojtek
    seq = read[1]
    keep=[1]*len(seq)
    pos = 0
    while pos <= len(seq)-window:
        dust = get_dust(seq[pos:pos+window], k)
        if dust > max_dust:
            for x in range(pos, pos+window):
                keep[x] = 0
        pos += k
    start = 0
    end = 0
    currstart = 0
    for k, g in itertools.groupby(keep):
        glen = len(list(g))
        if k==1:
            if glen > end-start:
                start = currstart
                end = currstart + glen
        currstart += glen
    return [read[0], read[1][start:end], read[2], read[3][start:end]]

def trim_window(read, qual=10, window=20, phred=33):
    # read input as [name, sequence, spacer, qualities]
    # phred+33 standard
    # qual input as integer
    # window length input as integer
    # adapted from wojtek
    rqual = [ord(x)-phred-qual for x in read[3]]
    start = len(rqual)
    end = 0
    for x in range(0, len(rqual)-window):
        avg = sum(rqual[x:x+window])/float(window)
        if avg > 0:
            start = x
            break
    for x in range(len(rqual), window, -1):
        avg = sum(rqual[x-window:x])/float(window)
        if avg > 0:
            end = x
            break
    if end <= start:
        return [read[0], "", read[2],  ""]
    return [read[0], read[1][start:end], read[2], read[3][start:end]]

def basic_quality_assurance_check_fastq_file(inputfile, outputfile):
    outfile = open(outputfile, "w")
    gen = Utility.read_generator_fastq_file(inputfile)

    block = next(gen, False)
    counter = 0
    omitted = 0
    while block:
        # some output every so many reads
        counter += 1
        if counter%1000==0:
            sys.stdout.write("Read %s sequences, of which %s were omitted.\r" % (counter, omitted))
            sys.stdout.flush()

        # sliding quality window
        block = trim_window(block)
        if block[1] == "":
            block = next(gen, False)
            omitted += 1
            continue

        # complexity check with DUST
        block = trim_dust(block)
        if block[1] == "":
            block = next(gen, False)
            omitted += 1
            continue

        # length check
        if len(block[1]) < 36:
            block = next(gen, False)
            omitted += 1
            continue

        # write out all reads that passed all 3 checks
        for index, each in enumerate(block):
            if index == 0:
                outfile.write("@"+each+"\n")
            else:
                outfile.write(each+"\n")

        block = next(gen, False)
    sys.stdout.write("Read %s sequences, of which %s were omitted.\n" % (counter, omitted))

def java_trimmer_wrapper(inputfiles, outputfiles, threads=1):
    """
    Wrapper for the Java Readtrimmer of Wojtek

    :param inputfiles: List of String paths to inputfiles
    :param outputfiles: List of String paths to outputfiles
    :return: List of String paths to outputfiles
    """

    # basic sanity checks
    assert 0 < len(inputfiles) < 3
    assert len(inputfiles) == len(outputfiles)

    # path to trimmer
    trimmer_path = "/home/andruscha/programs/readtrimmer/readtrimmer.jar"

    call = " ".join(["java -jar", trimmer_path, "-i", inputfiles[0], "-o", outputfiles[0],
                     "-m QUALITYWINDOW:window=20,quality=10;DUST:kmer=3,window=20,maxdust=5;MINLENGTH:length=36",
                     "-t", str(threads), "-T"])
    if len(inputfiles) == 2:
        call = " ".join([call, "-i2", inputfiles[1], "-o2", outputfiles[1]])

    sys.stdout.write("Invoking ReadTrimmer with "+call+"\n")
    trimmer = subprocess.Popen(call,shell=True)
    if trimmer.wait():  # errorcode not equal to 0
        raise SystemError("Trimmer failed to run ...")
